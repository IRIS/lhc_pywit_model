from lhc_pywit_model.lhc_model import LHCModel
from lhc_pywit_model.utils import execute_jobs_locally
from lhc_pywit_model.package_paths import data_directory

import matplotlib.pyplot as plt
import numpy as np
import os
import pyoptics
from scipy.constants import physical_constants, c


f_params_dict = {'start': 10,
                 'stop': 1e13,
                 'scan_type': 0,
                 'added': '1e-2 0.1 1 1e15',
                 'points_per_decade': 10,
                 'min_refine': 1e11,
                 'max_refine': 5e12,
                 'n_refine': 5000,
                 'sampling_exponent': 1e8,
                 }

additional_f_params = {'long_factor': 100,
                       'freq_lin_bisect': 1e11
                       }

z_params_dict = {'start': 0.01,
                 'stop': 1e13,
                 'scan_type': 2,
                 'added': ''
                 }

resonators_directory = os.path.join(data_directory, 'resonators')
broadband_directory = os.path.join(data_directory, 'broadband_resonators')
elliptic_elements_directory = os.path.join(data_directory, 'elliptic_elements')
optics_directory = os.path.join(data_directory, 'optics')
collimators_directory = os.path.join(data_directory, 'collimators')

optics_filename = os.path.join(optics_directory, 'twiss_lhcb1_beta130cm.tfs')
tfs = pyoptics.optics.open(optics_filename)
circ = tfs.header['length']
relativistic_gamma = tfs.header['gamma']
energy = relativistic_gamma * physical_constants['proton mass energy equivalent in MeV'][0] * 1e6
bending_radius = 2803.9
b_field_arcs = energy / (c * bending_radius)
# for the triplets we compute the B field computed from the energy with LHC bending radius, which is not really correct,
# but it has a small effect anyway...)
b_field_triplets = energy / (c * bending_radius)

input_params = {
    'optics_filename': optics_filename,
    'collimators_settings': os.path.join(collimators_directory, 'lhc_collimators_reference_b1.json'),
    'lxplusbatch': None,
    'compute_wake': False,
    'f_params_dict': f_params_dict,
    'z_params_dict': z_params_dict,
    'f_cutoff_bb': 50e9,
    'compute_geometric_impedance_collimators': True,
    'compute_taper_RW_impedance_collimators': True,
    'roman_pots_settings': None,
    'beam_screen_parameters_filename': os.path.join(elliptic_elements_directory, 'lhc_rw_parameters_beam_screen.json'),
    'machine': 'LHC',
    'various_elements_parameters_filename': os.path.join(elliptic_elements_directory,
                                                         'lhc_rw_parameters_various_elements.json'),
    'tapers_broadband_contributions_parameters_filename': os.path.join(broadband_directory,
                                                                       'lhc_triplets_tapers_broadband.json'),
    'bpms_broadband_contributions_parameters_filename':  os.path.join(broadband_directory,
                                                                      'lhc_triplets_bpms_broadband.json'),
    'add_design_broadband_contribution': True,
    'homs_parameters_filename_list': [os.path.join(resonators_directory, 'lhc_alice_homs.json'),
                                      os.path.join(resonators_directory, 'lhc_cms_homs.json'),
                                      os.path.join(resonators_directory, 'lhc_lhcb_homs.json'),
                                      os.path.join(resonators_directory, 'lhc_rf_cavities_homs.json')
                                      ],
    'mki_homs_parameters_filename': os.path.join(resonators_directory, 'lhc_mki_homs_2018_10_22_from_VV.json'),
    'b_field_arcs': b_field_arcs,
    'b_field_triplets': b_field_triplets,
    'jobs_submission_function': execute_jobs_locally
}

lhc_model = LHCModel(**input_params)

if input_params['lxplusbatch'] in [None, 'retrieve']:

    elem_tot = lhc_model.total
    disc_tot = elem_tot.get_component('x1000').discretize(10**3, 1, 1e-1, 1e13, freq_precision_factor=0.1)[0]
    inds_tot = np.argsort(disc_tot[0])
    plt.loglog(disc_tot[0][inds_tot], np.abs(disc_tot[1][inds_tot].real), 'b', label='Re, pywit model')
    plt.loglog(disc_tot[0][inds_tot], np.abs(disc_tot[1][inds_tot].imag), 'b--', label='Im, pywit model')

    Z_old = np.loadtxt('Zxdip_reference.dat', skiprows=1, dtype=float)

    plt.loglog(Z_old[:, 0], np.abs(Z_old[:, 1]), 'm', label='Re, old model')
    plt.loglog(Z_old[:, 0], np.abs(Z_old[:, 2]), 'm--', label='Im, old model')

    plt.title(r'$Z_{x}^{dip}$')
    plt.legend()
    plt.show()

