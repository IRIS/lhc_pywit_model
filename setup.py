from setuptools import setup
from setuptools.command.install import install


class PostInstallCommand(install):
    def run(self):
        install.run(self)
        # _initialize_pywib_directory()


setup(
    name='lhc_pywit_model',
    version='1.0.0',
    packages=['lhc_pywit_model'],
    url='https://gitlab.cern.ch/IRIS/lhc_iw_model',
    license='MIT',
    author='Lorenzo Giacomel, Nicolas Mounet',
    author_email='lorenzo.giacomel@cern.ch, nicolas.mounet@cern.ch',
    description='Impedance model of the LHC',
    cmdclass={'install': PostInstallCommand},
    include_package_data=True,

    package_data={'lhc_pywit_model': ['data/*', 'data/beam_screen_weld/*', 'data/resonators/*',
                                      'data/broadband_resonators/*', 'data/elliptic_elements/*',
                                      'data/machine_layouts/*', 'data/pumping_holes/*',
                                      'data/collimators/*', 'data/crab_cavities/*', 'data/stripline_bblrc/*',
                                      'data/optics/*']},
    install_requires=["numpy", "scipy", "matplotlib", "pyoptics"]
)
