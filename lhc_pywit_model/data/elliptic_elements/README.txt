Yokoya_factors_elliptic.dat: Yokoya factors for an elliptic structure in Yokoya_factors_elliptic.dat, taken from
E. Metral USPAS lecture (2009). The original source is K. Yokoya, KEK Preprint 92-196 (1993), Fig. 8.
