from __future__ import annotations

import numpy as np
import json
from typing import Dict, Union, Tuple, Callable
from pathlib import Path

from pywit.element import Element
from pywit.element import Component
from pywit.utilities import string_to_params, create_resonator_component, create_many_resonators_element
from collections import defaultdict

from lhc_pywit_model.parameters import DEFAULT_RESONATOR_F_ROI_LEVEL


class RFCavity(Element):
    """
    An element representing an RF cavity (longitudinal or transverse)
    """

    def __init__(self, rs_fundamental_mode: Dict[str, float], qs_fundamental_mode: Dict[str, float],
                 fs_fundamental_mode: Dict[str, float], homs_file_name: Union[str, Path] = None, length: float = 0,
                 beta_x: float = 0, beta_y: float = 0, apply_homs_frequency_spread: bool = False,
                 homs_spread_range: float = 3e6, homs_rng_seed: int = 0, add_transverse_feedback_x: bool = False,
                 add_transverse_feedback_y: bool = False, add_longitudinal_feedback: bool = False,
                 transverse_feedback_type_x: str = 'standard', transverse_feedback_type_y: str = 'standard',
                 longitudinal_feedback_type: str = 'standard', name: str = "Unnamed Element", tag: str = "",
                 description: str = "", gain_std_trans_fb: float = None, delay_std_trans_fb: float = None,
                 gain_comb_trans_fb: float = None, delay_comb_trans_fb: float = None, comb_bw_trans_fb: float = None,
                 comb_notch_bw_param_trans_fb: float = None, tune_x: float = None, tune_y: float = None,
                 t_rev: float = None, resonator_f_roi_level: float = DEFAULT_RESONATOR_F_ROI_LEVEL, ncav: int = 1,
                 rotation_angle_homs: float = 0):
        """
        :param rs_fundamental_mode: A dictionary where the keys correspond to a plane followed by four exponents, i.e.
        "y0100", and the values give the shunt impedance corresponding to this particular component.
        :param qs_fundamental_mode: A dictionary where the keys correspond to a plane followed by four exponents, i.e.
        "y0100", and the values give the quality factor corresponding to this particular component.
        :param fs_fundamental_mode: A dictionary where the keys correspond to a plane followed by four exponents, i.e.
        "y0100", and the values give the resonant frequency corresponding to this particular component.
        :param homs_file_name: The path to the file containing the parameters of the high-order modes, i.e. a json file
        containing a list of dictionaries with the following keys:
            - Rl, the longitudinal shunt impedance in Ohm;
            - Ql, the longitudinal Q-factor (a-dimensional);
            - fl, the resonant frequency of the longitudinal component;
            - Rxd, the shunt impedance of the x component in Ohm;
            - Qxd, the Q-factor of the x component(a-dimensional);
            - fxd, the resonant frequency of the x component in Hz;
            - Rxd, the shunt impedance of the y component in Ohm;
            - Qxd, the Q-factor of the y component (a-dimensional);
            - fyd, the resonant frequency of the x component in Hz;
        :param length: The length of the cavity.
        :param beta_x: The size of the beta function in the x-plane at the position of the Element, must be specified
        for valid initialization.
        :param beta_y: The size of the beta function in the y-plane at the position of the Element, must be specified
        for valid initialization.
        :param apply_homs_frequency_spread: A flag specifying if the high-order modes frequencies should be randomly
        shifted. If it is True, then the modes are shifted by a random number drawn from a uniform distribution in
        (0, homs_spread_range).
        :param homs_spread_range: The spread high-order modes frequencies (see above).
        :param add_transverse_feedback_x: A flag specifying if an RF feedback should be added to the x-component of the
        fundamental mode.
        :param add_transverse_feedback_y: A flag specifying if an RF feedback should be added to the y-component of the
        fundamental mode.
        :param add_longitudinal_feedback: A flag specifying if an RF feedback should be added to the z-component of the
        fundamental mode. WARNING: the longitudinal feedback is not implemented yet and it cannot be specified.
        :param transverse_feedback_type_x: The type of transverse feedback to be applied to the x-component of the
        fundamental mode. It can be 'standard' for the standard closed-loop RF feedback or 'comb' for the betatron comb
        filter feedback.
        :param transverse_feedback_type_y: The type of transverse feedback to be applied to the y-component of the
        fundamental mode. It can be 'standard' for the standard closed-loop RF feedback or 'comb' for the betatron comb
        filter feedback.
        :param longitudinal_feedback_type: The type of transverse feedback to be applied to the z-component of the
        fundamental mode. WARNING: the longitudinal feedback is not implemented yet and it cannot be specified.
        :param name: A user-specified name of the cavity.
        :param tag: A string corresponding to a specific component
        :param description: A string describing the cavity
        :param gain_std_trans_fb: The gain of the standard transverse RF feedback. It must be specified when any
        feedback system is used.
        :param delay_std_trans_fb: The delay of the standard transverse RF feedback. It must be specified when any
        feedback system is used.
        :param gain_comb_trans_fb: The gain of the comb filter of the transverse RF feedback. It must be specified if
        transverse_feedback_type_x or transverse_feedback_type_y is 'comb'.
        :param delay_comb_trans_fb: The delay of the comb filter of the transverse RF feedback. It must be specified if
        transverse_feedback_type_x or transverse_feedback_type_y is 'comb'.
        :param comb_bw_trans_fb: The bandwidth of the comb filter of the transverse RF feedback, i.e. the width of the
        comb in Hz. It must be specified if transverse_feedback_type_x or transverse_feedback_type_y is 'comb'.
        :param comb_notch_bw_param_trans_fb: An a-dimensional parameter between 0 and 1 specifying the width of the
        notches of the betatron comb filter. It must be specified if transverse_feedback_type_x or
        transverse_feedback_type_y is 'comb'.
        :param tune_x: The machine tune (the fractional part is enough!) in the x-plane. It must be specified if
        transverse_feedback_type_x is 'comb'.
        :param tune_y: The machine tune (the fractional part is enough!) in the x-plane. It must be specified if
        transverse_feedback_type_y is 'comb'.
        :param t_rev: The machine revolution frequency.
        :param ncav: number of cavities: the shunt impedance of the fundamnetal mode is multiplied by ncav and the homs
        are added ncav times
        :param rotation_angle_homs: an angle by which the homs are rotated
        """

        if apply_homs_frequency_spread and homs_spread_range is None:
            raise ValueError('When apply_homs_frequency_spread is true, homs_spread_range must be specified.')

        if add_transverse_feedback_x:
            if transverse_feedback_type_x != 'standard' and transverse_feedback_type_x != 'comb':
                raise ValueError("transverse_feedback_type_x must be 'standard' or 'comb'")

            if transverse_feedback_type_x == 'standard':
                if gain_std_trans_fb is None or delay_std_trans_fb is None:
                    raise ValueError("if transverse_feedback_type_x is 'standard', gain_std_trans_fb and "
                                     'delay_std_trans_fb must be specified')

            if transverse_feedback_type_x == 'comb':
                if (gain_std_trans_fb is None or delay_std_trans_fb is None or gain_comb_trans_fb is None or
                        delay_comb_trans_fb is None or comb_bw_trans_fb is None or
                        comb_notch_bw_param_trans_fb is None or tune_x is None or t_rev is None):
                    raise ValueError("if transverse_feedback_type_x is 'comb', gain_std_trans_fb, "
                                     "delay_std_trans_fb, gain_comb_trans_fb, delay_comb_trans_fb, comb_bw_trans_fb,"
                                     "comb_notch_bw_param_trans_fb tune_x and t_rev must be specified")

        if add_transverse_feedback_y:
            if transverse_feedback_type_y != 'standard' and transverse_feedback_type_y != 'comb':
                raise ValueError("transverse_feedback_type_y must be 'standard' or 'comb'")

            if transverse_feedback_type_y == 'standard':
                if gain_std_trans_fb is None or delay_std_trans_fb is None:
                    raise ValueError("if transverse_feedback_type_y is 'standard', gain_std_trans_fb and "
                                     'delay_std_trans_fb must be specified')

            if transverse_feedback_type_y == 'comb':
                if (gain_std_trans_fb is None or delay_std_trans_fb is None or gain_comb_trans_fb is None or
                        delay_comb_trans_fb is None or comb_bw_trans_fb is None or
                        comb_notch_bw_param_trans_fb is None or tune_y is None or t_rev is None):
                    raise ValueError("if transverse_feedback_type_y is 'comb', gain_std_trans_fb, "
                                     "delay_std_trans_fb, gain_comb_trans_fb, delay_comb_trans_fb, comb_bw_trans_fb,"
                                     "comb_notch_bw_param_trans_fb tune_y and t_rev must be specified")

        components_fundamental = []
        for key in rs_fundamental_mode.keys():
            plane, exponents = string_to_params(key, include_is_impedance=False)
            if plane == 'x' and add_transverse_feedback_x:
                if transverse_feedback_type_x == 'standard':
                    self.gain_std_trans_fb = gain_std_trans_fb
                    self.delay_std_trans_fb = delay_std_trans_fb
                    self.gain_comb_trans_fb = 0
                    self.delay_comb_trans_fb = 0
                    self.comb_bw_trans_fb = 0
                    self.comb_notch_bw_param_trans_fb = 0
                elif transverse_feedback_type_x == 'comb':
                    self.gain_std_trans_fb = gain_std_trans_fb
                    self.delay_std_trans_fb = delay_std_trans_fb
                    self.gain_comb_trans_fb = gain_comb_trans_fb
                    self.delay_comb_trans_fb = delay_comb_trans_fb
                    self.comb_bw_trans_fb = comb_bw_trans_fb
                    self.comb_notch_bw_param_trans_fb = comb_notch_bw_param_trans_fb
                else:
                    raise ValueError('transverse_feedback_type_x can only be standard or comb')

                components_fundamental.append(betatron_comb_filter_fb_transverse_component(
                    r=rs_fundamental_mode[key] * ncav,
                    q=qs_fundamental_mode[key],
                    f=fs_fundamental_mode[key],
                    gain=self.gain_std_trans_fb,
                    delay=self.delay_std_trans_fb,
                    comb_gain=self.gain_comb_trans_fb,
                    comb_delay=self.delay_comb_trans_fb,
                    comb_bw=self.comb_bw_trans_fb, tune=tune_x,
                    t_rev=t_rev,
                    notch_bw_param=self.comb_notch_bw_param_trans_fb,
                    plane=plane,
                    source_exponents=exponents[0: 2],
                    test_exponents=exponents[2:],
                ))

            elif plane == 'y' and add_transverse_feedback_y:

                if transverse_feedback_type_y == 'standard':
                    self.gain_std_trans_fb = gain_std_trans_fb
                    self.delay_std_trans_fb = delay_std_trans_fb
                    self.gain_comb_trans_fb = 0
                    self.delay_comb_trans_fb = 0
                    self.comb_bw_trans_fb = 0
                    self.comb_notch_bw_param_trans_fb = 0
                elif transverse_feedback_type_y == 'comb':
                    self.gain_std_trans_fb = gain_std_trans_fb
                    self.delay_std_trans_fb = delay_std_trans_fb
                    self.gain_comb_trans_fb = gain_comb_trans_fb
                    self.delay_comb_trans_fb = delay_comb_trans_fb
                    self.comb_bw_trans_fb = comb_bw_trans_fb
                    self.comb_notch_bw_param_trans_fb = comb_notch_bw_param_trans_fb
                else:
                    raise ValueError('transverse_feedback_type_y can only be standard or comb')

                components_fundamental.append(betatron_comb_filter_fb_transverse_component(
                    r=rs_fundamental_mode[key] * ncav,
                    q=qs_fundamental_mode[key],
                    f=fs_fundamental_mode[key],
                    gain=self.gain_std_trans_fb,
                    delay=self.delay_std_trans_fb,
                    comb_gain=self.gain_comb_trans_fb,
                    comb_delay=self.delay_comb_trans_fb,
                    comb_bw=self.comb_bw_trans_fb, tune=tune_y,
                    t_rev=t_rev,
                    notch_bw_param=self.comb_notch_bw_param_trans_fb,
                    plane=plane,
                    source_exponents=exponents[0: 2],
                    test_exponents=exponents[2:],
                ))

            elif plane == 'z' and add_longitudinal_feedback:
                print(longitudinal_feedback_type)
                raise ValueError('longitudinal feedback not yet implemented')

            else:
                components_fundamental.append(create_resonator_component(plane=plane,
                                                                         exponents=exponents,
                                                                         r=rs_fundamental_mode[key] * ncav,
                                                                         q=qs_fundamental_mode[key],
                                                                         f_r=fs_fundamental_mode[key],
                                                                         f_roi_level=resonator_f_roi_level))

        comp_dict = defaultdict(int)
        for c in components_fundamental:
            comp_dict[(c.plane, c.source_exponents, c.test_exponents)] += c

        if homs_file_name is not None:
            with open(homs_file_name) as homs_file:
                homs_dict = json.load(homs_file)

            params_dict = {'z0000': [], 'x1000': [], 'y0100': []}

            np.random.seed(homs_rng_seed)

            for nc in range(ncav):
                for mode_parameters in homs_dict['modes']:
                    if apply_homs_frequency_spread:
                        # We check if all modes have the same frequency in x,y and z (as it is the case for the Crab
                        # Cavities). If this is True we will apply the same frequency delta in x,y and z (still
                        # different delta for each mode)
                        if not (mode_parameters['fl'] == mode_parameters['fxd'] == mode_parameters['fyd']):
                            frequency_delta_x = np.random.uniform(low=-homs_spread_range, high=homs_spread_range)
                            frequency_delta_y = np.random.uniform(low=-homs_spread_range, high=homs_spread_range)
                            frequency_delta_z = np.random.uniform(low=-homs_spread_range, high=homs_spread_range)
                        else:
                            frequency_delta_x = np.random.uniform(low=-homs_spread_range, high=homs_spread_range)
                            frequency_delta_y = frequency_delta_x
                            frequency_delta_z = frequency_delta_x
                    else:
                        frequency_delta_x = 0
                        frequency_delta_y = 0
                        frequency_delta_z = 0

                    params_dict['z0000'].append(
                        {
                            'r': float(mode_parameters['Rl']),
                            'q': float(mode_parameters['Ql']),
                            'f': float(mode_parameters['fl']) + frequency_delta_z,
                            'f_roi_level': resonator_f_roi_level
                        }
                    )

                    params_dict['x1000'].append(
                        {
                            'r': float(mode_parameters['Rxd']),
                            'q': float(mode_parameters['Qxd']),
                            'f': float(mode_parameters['fxd']) + frequency_delta_x,
                            'f_roi_level': resonator_f_roi_level
                        }
                    )

                    params_dict['y0100'].append(
                        {
                            'r': float(mode_parameters['Ryd']),
                            'q': float(mode_parameters['Qyd']),
                            'f': float(mode_parameters['fyd']) + frequency_delta_y,
                            'f_roi_level': resonator_f_roi_level
                        }
                    )

            many_res_elem = create_many_resonators_element(length=length, beta_x=beta_x, beta_y=beta_y,
                                                           params_dict=params_dict)
            f_rois_x = many_res_elem.get_component('x1000').f_rois
            f_rois_y = many_res_elem.get_component('y0100').f_rois
            f_rois_z = many_res_elem.get_component('z0000').f_rois

            many_res_elem_rot = many_res_elem.rotated(rotation_angle_homs)

            if rotation_angle_homs == np.pi / 2:
                many_res_elem_rot.get_component('x1000').f_rois = f_rois_y
                many_res_elem_rot.get_component('y0100').f_rois = f_rois_x
                many_res_elem_rot.get_component('z0000').f_rois = f_rois_z
            elif rotation_angle_homs == 0:
                many_res_elem_rot.get_component('x1000').f_rois = f_rois_x
                many_res_elem_rot.get_component('y0100').f_rois = f_rois_y
                many_res_elem_rot.get_component('z0000').f_rois = f_rois_z
            else:
                many_res_elem_rot.get_component('x1000').f_rois = f_rois_x + f_rois_y
                many_res_elem_rot.get_component('y0100').f_rois = f_rois_x + f_rois_y
                many_res_elem_rot.get_component('z0000').f_rois = f_rois_z

            components_hom = many_res_elem_rot.components

            for c in components_hom:
                comp_dict[(c.plane, c.source_exponents, c.test_exponents)] += c

        components = sorted(comp_dict.values(), key=lambda x: (x.plane, x.source_exponents, x.test_exponents))

        super().__init__(length=length, beta_x=beta_x, beta_y=beta_y, components=components, name=name, tag=tag,
                         description=description)


def baseband_response(omega, tune, t_rev, gain, notch_bw_param, comb_bw) -> float:
    flag_inside = (omega / (2 * np.pi) > -comb_bw / 2) * (omega / (2 * np.pi) < comb_bw / 2)
    gain_factor = gain * (1 - notch_bw_param)
    term_plus = ((np.exp(1j * 2 * np.pi * tune) * np.exp(-1j * t_rev * omega)) /
                 (1 - notch_bw_param * np.exp(1j * 2 * np.pi * tune) * np.exp(-1j * t_rev * omega)))
    term_minus = ((np.exp(-1j * 2 * np.pi * tune) * np.exp(-1j * t_rev * omega)) /
                  (1 - notch_bw_param * np.exp(-1j * 2 * np.pi * tune) * np.exp(-1j * t_rev * omega)))
    return flag_inside * gain_factor * (term_plus + term_minus)


def betatron_comb_filter_response(omega, omega_r, tune, t_rev, gain, notch_bw_param, comb_bw) -> float:
    return (baseband_response(omega - omega_r, tune, t_rev, gain, notch_bw_param, comb_bw) +
            baseband_response(omega + omega_r, tune, t_rev, gain, notch_bw_param, comb_bw)) / 2


def betatron_comb_filter_fb_impedance_function(freq: float, long_impedance: Callable[[float], float],
                                               trans_impedance: Callable[[float], float], r: float, f_r: float,
                                               gain: float, delay: float, comb_gain: float, comb_delay: float,
                                               comb_bw: float, tune: float, t_rev: float,
                                               notch_bw_param: float) -> float:
    omega = 2 * np.pi * freq
    omega_r = f_r * 2 * np.pi

    bcf_response = betatron_comb_filter_response(omega=omega, omega_r=omega_r, tune=tune, t_rev=t_rev, gain=comb_gain,
                                                 notch_bw_param=notch_bw_param, comb_bw=comb_bw)

    return trans_impedance(freq) / (1 +
                                    (gain * long_impedance(freq) * np.exp(
                                        1j * delay * 2 * np.pi * (-freq + f_r * np.sign(freq)))) *
                                    (1 + bcf_response * np.exp(
                                        1j * comb_delay * 2 * np.pi * (freq - f_r * np.sign(freq))))
                                    )


def betatron_comb_filter_fb_transverse_component(r: float, q: float, f: float, gain: float, delay: float,
                                                 comb_gain: float, comb_delay: float, comb_bw: float, tune: float,
                                                 t_rev: float, notch_bw_param: float, plane: str,
                                                 source_exponents: Tuple[int, int],
                                                 test_exponents: Tuple[int, int]) -> Component:
    long_comp = create_resonator_component(plane='z', exponents=(0, 0, 0, 0), r=1, q=q, f_r=f)

    trans_comp = create_resonator_component(plane=plane, exponents=source_exponents + test_exponents, r=r, q=q, f_r=f)

    # For the roi we place 30 rois in (f - 5e6, f - comb_bw/2), 30 rois in (f + comb_bw/2, f + 5e6) and 300 rois in
    # (f - comb_bw/2, f + comb_bw/2). With this strategy the filter is well resolved as long as each small roi consists
    # of least ~100 points
    n_rois_outside_bw = 30
    distance_from_f_r = 5e6
    width_single_roi = (distance_from_f_r - comb_bw / 2) / n_rois_outside_bw
    f_rois = [(f - distance_from_f_r + i * width_single_roi,
               f - distance_from_f_r + (i + 1) * width_single_roi) for i in range(n_rois_outside_bw)]
    f_rois += [(f + comb_bw / 2 + i * width_single_roi,
                f + comb_bw / 2 + (i + 1) * width_single_roi) for i in range(n_rois_outside_bw)]
    n_rois_intside_bw = 300
    width_single_roi = comb_bw / n_rois_intside_bw
    f_rois += [(f - comb_bw / 2 + i * width_single_roi,
                f - comb_bw / 2 + (i + 1) * width_single_roi) for i in range(n_rois_intside_bw)]

    return Component(impedance=lambda x: betatron_comb_filter_fb_impedance_function
                     (
                         freq=x,
                         long_impedance=long_comp.impedance,
                         trans_impedance=trans_comp.impedance,
                         r=r, f_r=f, gain=gain, delay=delay,
                         comb_gain=comb_gain, comb_delay=comb_delay,
                         comb_bw=comb_bw, tune=tune, t_rev=t_rev,
                         notch_bw_param=notch_bw_param
                     ),
                     wake=lambda x: 0,
                     plane=plane, source_exponents=source_exponents,
                     test_exponents=test_exponents,
                     f_rois=f_rois)
