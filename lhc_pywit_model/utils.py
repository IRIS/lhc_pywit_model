from __future__ import annotations

import os
import json

from typing import Union, List, Dict, Tuple
from pathlib import Path

import datetime
from copy import deepcopy

import pyoptics as opt

import numpy as np
from scipy.interpolate import interp1d
from scipy.constants import c
import csv

from pywit.component import Component
from pywit.element import Element
from pywit.interface import Layer, get_iw2d_config_value
from pywit.materials import layer_from_json_material_library, copper_at_temperature

import lhc_pywit_model.data.machine_layouts.s_coordinates_lhc_b1_layout2021_elements as lhc_b1_layout
import lhc_pywit_model.data.machine_layouts.s_coordinates_lhc_b2_layout2021_elements as lhc_b2_layout
import lhc_pywit_model.data.machine_layouts.s_coordinates_hllhc_b1_v1p5_elements as hllhc_b1_layout
import lhc_pywit_model.data.machine_layouts.s_coordinates_hllhc_b2_v1p5_elements as hllhc_b2_layout
from lhc_pywit_model.package_paths import data_directory

try:
    import pytimber

    pytimber_imported = True
except ImportError:
    pytimber_imported = False


def execute_jobs_locally(hashes_dict: Dict[Union[str, bool]],
                         sub_file_name: Union[str, Path]):

    projects_path = Path(get_iw2d_config_value('project_directory'))

    path_here = os.getcwd()

    for name in hashes_dict:
        print(name)
        coll_hash = hashes_dict[name]['input_hash']
        already_computed = hashes_dict[name]['already_computed']
        if already_computed:
            print('already computed')
            continue
        path = os.path.join(projects_path, coll_hash[0:2],
                            coll_hash[2:4], coll_hash[4:], "iw2d_job.txt")
        os.chdir(os.path.dirname(path))
        os.system(f"sh {path}")
        os.chdir(path_here)


def execute_jobs_on_htcondor(hashes_dict: Dict[Union[str, bool]],
                             sub_file_name: Union[str, Path]):

    projects_path = Path(get_iw2d_config_value('project_directory'))
    collimators_folders_list_file = os.path.join(projects_path,
                                                 'list_collimators.txt')
    all_coll_dir = ''

    for name in hashes_dict:
        already_computed = hashes_dict[name]['already_computed']
        if already_computed:
            print('already computed')
            continue

        input_hash = hashes_dict[name]
        coll_dir = os.path.join(projects_path, input_hash[0:2],
                                input_hash[2:4], input_hash[4:])
        all_coll_dir += str(coll_dir) + '\n'

    if len(all_coll_dir) == 0:
        print('Nothing new to simulate')
        return

    # Create the file containing the list of collimators folders
    with open(collimators_folders_list_file, 'w') as f:
        f.writelines(all_coll_dir)

    text = f"executable = $(folder)/iw2d_job.txt\n" \
           f"ID = $(Cluster).$(Process)\n" \
           f"output = $(folder)/$(ID).out\n" \
           f"error = $(folder)/$(ID).err\n" \
           f"log = $(folder)/$(ID).log\n" \
           f"universe = vanilla\n" \
           f"initialdir = $(folder)\n" \
           f'+JobFlavour = "tomorrow"\n' \
           f'queue folder from {collimators_folders_list_file}'

    with open(sub_file_name, 'w') as file:
        file.write(text)

    os.system('condor_submit ' + sub_file_name)


def execute_jobs_on_slurm(hashes_dict: Dict[str, Dict[str, Union[bool, str]]],
                          sub_file_name: Union[str, Path]):

    projects_path = Path(get_iw2d_config_value('project_directory'))

    for name in hashes_dict:
        print(name)
        input_hash = hashes_dict[name]['input_hash']
        already_computed = hashes_dict[name]['already_computed']
        if already_computed:
            print('already computed')
            continue
        path = os.path.join(projects_path, input_hash[0:2], input_hash[2:4],
                            input_hash[4:])

        text = f"#!/bin/bash\n" \
               f"#SBATCH --job-name={name}\n" \
               f"#SBATCH --output {name}.out\n" \
               f"\n" \
               f"cd {path}\n" \
               f"srun sh iw2d_job.txt\n"

        with open(os.path.join(path, 'sjob_iw2d'), 'w') as file:
            file.write(text)

        os.chdir(path)
        os.system('sbatch sjob_iw2d')


def create_list(a, n=1):
    """
    if a is a scalar, return a list containing n times the element a
    otherwise return a
    """
    if not (np.isscalar(a)):
        pass
    else:
        b = []
        for i in range(n):
            b.append(a)
        a = deepcopy(b)

    return a


def read_column_from_file(filename, attribute, data_type):
    column = []
    with open(filename) as csvfile:
        dict_reader = csv.DictReader(csvfile, delimiter='\t')
        for row in dict_reader:
            column.append(data_type(row[attribute]))

    return np.array(column, dtype=data_type)


def collimators_table_to_json(filename, output_filename, tctp_mode_filename='TCTPmode_vs_fullgap.txt'):
    collimators_table = np.loadtxt(filename, dtype=str)
    header = collimators_table[0, :]
    names_ind = np.where(header == 'Name')[0]
    angle_ind = np.where(header == 'Angle[rad]')[0]
    material_ind = np.where(header == 'Material')[0]
    thickness_ind = np.where(header == 'Thickness[m]')[0]
    halfgap_ind = np.where(header == 'Halfgap[m]')[0]
    length_ind = np.where(header == 'Length[m]')[0]
    tilt_1_ind = np.where(header == 'tilt1[rad]')[0]
    tilt_2_ind = np.where(header == 'tilt2[rad]')[0]
    taper_ind = np.where(header == 'Taper')[0]
    nsig_ind = np.where(header == 'nsig')[0]
    specified_taper = len(taper_ind) > 0
    names = collimators_table[1:, names_ind]

    dict_colls = {}
    for i, n in enumerate(names):
        i_layer = i + 1
        name = n[0]
        print(name)

        material_str = collimators_table[i_layer, material_ind][0]
        if material_str.lower() == 'ti6al4v':
            material = 'Ti-6Al-4V'
        elif material_str.lower() == 'cu':
            material = 'Cu'
        elif material_str.lower() == 'cuhipims':
            material = 'cu_hipims'
        elif material_str.lower() == 'mo':
            material = 'Mo'
        elif material_str.lower() == 'c':
            material = 'graphite'
        else:
            material = material_str.lower()

        if not dict_colls.__contains__(name):

            taper_type = 'tcs'
            if specified_taper:
                taper = str(collimators_table[i_layer, taper_ind][0])

                taper_type = taper.lower()

                if taper == 'TCL' and name.startswith('TCLPX'):
                    taper_type = 'tctpx'

                elif taper == 'TCL' and material == 'Cu':
                    taper_type = 'tcl'

                elif taper in ['TCLA', 'TCL', 'TCLD', 'TDIS']:
                    taper_type = 'tcla'

                elif taper == 'TCSP' and name.startswith('TCTPX'):
                    taper_type = 'tctpx'

                elif taper == 'TCSP' and name.startswith('TCTP'):
                    taper_type = 'tctp'

                elif taper == 'TCSPM' and name.startswith('TCTP'):
                    taper_type = 'tctpm'

                elif taper == 'TCSPM' and material == 'cu_hipims':
                    taper_type = 'tcspm_cu'

                elif taper == 'TCS':
                    taper_type = 'tcs'

                elif taper == 'NONE' and name.startswith('TCDQ'):
                    taper_type = 'tcdq'

                elif taper == 'TCP':
                    taper_type = 'tcp'

            dict_colls[name] = {
                'angle': float(collimators_table[i_layer, angle_ind][0]),
                'layers': [
                    {
                        'material': material,
                        'thickness': float(collimators_table[i_layer, thickness_ind][0])
                    }
                ],
                'halfgap': float(collimators_table[i_layer, halfgap_ind][0]),
                'length': float(collimators_table[i_layer, length_ind][0]),
                'taper_type': taper_type,
                'tilt_1': float(collimators_table[i_layer, tilt_1_ind][0]),
                'tilt_2': float(collimators_table[i_layer, tilt_2_ind][0]),
                'n_sigma': float(collimators_table[i_layer, nsig_ind][0])
            }

            if (name.startswith('TDI') and
                    (name.endswith('.1') or name.endswith('.2') or name.endswith('.3'))):
                dict_colls[name]['name_for_twiss_file'] = name[:-2]

            if taper_type in ['tctp', 'tcsp']:
                dict_colls[name]['halfgap_dependent_hom_files'] = [tctp_mode_filename]
            else:
                dict_colls[name]['halfgap_dependent_hom_files'] = []
        else:
            dict_colls[name]['layers'].append({
                'material': material,
                'thickness': float(collimators_table[i_layer, thickness_ind][0])
            })

        if name.startswith('TCDQ'):
            dict_colls[name]['asymmetry_factor'] = 0

    output_file = open(output_filename, "w")

    json.dump(dict_colls, output_file, indent=2)

    return dict_colls


def table_to_json(filename, output_filename):
    table = np.loadtxt(filename, dtype=str)
    header = table[0, :]

    names_ind = np.where(header == 'Name')[0]
    if len(names_ind) == 0:
        names_ind = np.where(header == 'name')[0]

    if len(names_ind) > 0:
        names_ind = names_ind[0]

        names = table[1:, names_ind]

    else:
        # we can't use np.arange because it uses np.int64 type which cannot be used as a json key
        names = [int(i) + 1 for i in range(table.shape[0] - 1)]

    dict_out = {}

    for i_name, name in enumerate(names):
        print(name)
        dict_out[name] = {}

        for i_attribute, attribute in enumerate(header):
            if attribute == 'Name' or attribute == 'name':
                continue
            else:
                try:
                    dict_out[name][attribute] = float(table[i_name + 1, i_attribute])
                except ValueError:
                    dict_out[name][attribute] = table[i_name + 1, i_attribute]

    with open(output_filename, "w") as output_file:
        json.dump(dict_out, output_file, indent=2)


def multiply_component_by_frequency_dependent_function(c: Component, scalar_function_real: np.ndarray,
                                                       scalar_function_imag:
                                                       np.ndarray, frequencies: np.ndarray) -> Component:
    """
    Implements the multiplication of a component by a frequency-dependent scalar.
    WARNING: it can create wake-impedance inconsistencies
    :param c: the component to be multiplied
    :param scalar_function_real: The function by which the real part of the component is multiplied, represented
    discretely as a vector
    :param scalar_function_imag: The function by which the imaginary part of the component is multiplied, represented
    discretely as a vector
    :param frequencies: the frequencies at which the discretized functions are evaluated
    :return: A newly initialized Component identical to self in every way apart from the impedance function, which has
    been multiplied by the frequency dependent scalar.
    """
    # Initializes and returns a new Component with attributes like self, apart from the scaled functions
    return Component((lambda x: (np.interp(x, frequencies, scalar_function_real) *
                                 np.real(c.impedance(x))) +
                                (1j * np.interp(x, frequencies, scalar_function_imag) *
                                 np.imag(c.impedance(x)))) if c.impedance else None,
                     c.wake, c.plane, c.source_exponents, c.test_exponents, c.name, c.f_rois, c.t_rois)


def multiply_component_by_time_dependent_function(c: Component, scalar_function: np.ndarray,
                                                  times: np.ndarray) -> Component:
    """
    Implements the multiplication of a component by a frequency-dependent scalar.
    WARNING: it can create wake-impedance inconsistencies
    :param c: the component to be multiplied
    :param scalar_function: The function by which the component is multiplied, represented discretely as a vector
    :param times: the times at which the discretized function is evaluated
    :return: A newly initialized Component identical to self in every way apart from the impedance function, which has
    been multiplied by the frequency dependent scalar.
    """
    # Initializes and returns a new Component with attributes like self, apart from the scaled functions
    return Component(c.impedance,
                     (lambda x: np.interp(x, times, scalar_function) * c.impedance(x)) if c.impedance else None,
                     c.plane, c.source_exponents, c.test_exponents, c.name, c.f_rois, c.t_rois)


def multiply_element_by_frequency_dependent_function(element: Element, scalar_functions_real_list: List[np.ndarray],
                                                     scalar_functions_imag_list: List[np.ndarray],
                                                     frequencies: np.ndarray,
                                                     test_exponents_list: List[tuple],
                                                     source_exponents_list: List[tuple]) -> Element:
    """
    Implements the multiplication of an Element by a frequency-dependent scalar, which can be different for each
    component.
    WARNING: it can create wake-impedance inconsistencies
    :param element: the element to be multiplied
    :param scalar_functions_real_list: the list of functions by which the real part of the component is multiplied,
    represented discretely as a vector
    :param scalar_functions_imag_list: the list of functions by which the imaginary part of the component is multiplied,
    represented discretely as a vector
    :param frequencies: the frequencies corresponding at which the discretized functions are evaluated
    :param test_exponents_list: the list of test exponents of components which have to be multiplied
    :param source_exponents_list: the list of source exponents of components which have to be multiplied
    :return: A newly initialized element which has the same beta_x and beta_y values as element but has some components
    multiplied by the frequency-dependent scalar
    """
    assert len(scalar_functions_real_list) == len(test_exponents_list) == len(source_exponents_list)
    assert len(scalar_functions_imag_list) == len(test_exponents_list) == len(source_exponents_list)
    new_components = []
    for comp in element.components:
        multiplied_component = comp
        # Check if this component needs to be multiplied
        for i_component, scalar_function_real in enumerate(scalar_functions_real_list):
            test_exponents = test_exponents_list[i_component]
            source_exponents = source_exponents_list[i_component]
            scalar_function_imag = scalar_functions_imag_list[i_component]
            if comp.test_exponents == test_exponents and comp.source_exponents == source_exponents:
                # Do the multiplication
                multiplied_component = multiply_component_by_frequency_dependent_function(
                    c=comp,
                    scalar_function_real=scalar_function_real,
                    scalar_function_imag=scalar_function_imag,
                    frequencies=frequencies
                )

        # Append the new component to the list
        new_components.append(multiplied_component)

    return Element(element.length, element.beta_x, element.beta_y, new_components, element.name, element.tag,
                   element.description)


def multiply_element_by_time_dependent_function(element: Element, scalar_functions_list: List[np.ndarray],
                                                times: np.ndarray,
                                                test_exponents_list: List[tuple],
                                                source_exponents_list: List[tuple]) -> Element:
    """
    Implements the multiplication of an Element by a time-dependent scalar, which can be different for each
    component.
    WARNING: it can create wake-impedance inconsistencies
    :param element: the element to be multiplied
    :param times: the times at which the discretized function is evaluated
    :param scalar_functions_list: the list of functions by which the components need to be multiplied
    :param test_exponents_list: the list of test exponents of components which have to be multiplied
    :param source_exponents_list: the list of source exponents of components which have to be multiplied
    :return: A newly initialized element which has the same beta_x and beta_y values as element but has some components
    multiplied by the time-dependent scalar
    """
    assert len(scalar_functions_list) == len(test_exponents_list) == len(source_exponents_list)
    new_components = []
    for comp in element.components:
        multiplied_component = comp
        # Check if this component needs to be multiplied
        for i_component, scalar_function in enumerate(scalar_functions_list):
            test_exponents = test_exponents_list[i_component]
            source_exponents = source_exponents_list[i_component]
            if comp.test_exponents == test_exponents and comp.source_exponents == source_exponents:
                # Do the multiplication
                multiplied_component = multiply_component_by_time_dependent_function(c=comp,
                                                                                     scalar_function=scalar_function,
                                                                                     times=times)

        # Append the new component to the list
        new_components.append(multiplied_component)

    return Element(element.length, element.beta_x, element.beta_y, new_components, element.name, element.tag,
                   element.description)


def create_layer_from_dict(layer_dict: dict, b_field: float = None) -> Layer:
    material = layer_dict["material"]

    try:
        # check the lhc_pywit_model materials
        layer = layer_from_json_material_library(thickness=layer_dict['thickness'], material_key=material.lower(),
                                                 library_path=Path(os.path.join(os.path.dirname(__file__),
                                                                                'data/materials.json')))
    except AssertionError:
        try:
            # check the pywit materials
            layer = layer_from_json_material_library(thickness=layer_dict['thickness'], material_key=material)

        except AssertionError:
            # the python library only contains copper at temperature
            if not (material.startswith('Cu') and material[-1] == 'K'):
                raise ValueError(
                    f'create_layer_from_dict: could not find material {layer_dict["material"]} neither in Python '
                    f'nor in json material libraries')

            # we need to extract the temperature
            temperature = float(material[2:-1])
            try:
                if b_field is not None:
                    layer = copper_at_temperature(thickness=layer_dict['thickness'], T=temperature, B=b_field)
                else:
                    layer = copper_at_temperature(thickness=layer_dict['thickness'], T=temperature)

            except AttributeError:
                raise ValueError(
                    f'create_layer_from_dict: could not find material {layer_dict["material"]} neither in Python '
                    f'nor in json material libraries')

    return layer


def compute_betas_and_lengths(optics_filename: Union[Path, str], flag_hllhc: bool = False,
                              length_collimators: float = 57) -> Dict[str, Dict[str, float]]:
    """
    Compute for each LHC element the beta functions and length (except for the collimators).
    :param optics_filename: the path to a twiss file with the optics
    :param optics_filename: the element to be multiplied
    :param flag_hllhc: if True use the HL-LHC layout, otherwise use the LHC one
    :param length_collimators: the total length of the collimators (it is removed from the warm pipe length)
    :return a dictionary associating to each element a dictionary containing the beta_x and beta_y functions and the
            length of the element (if there are multiple devices with the same broadband impedance, e.g. many RF
            cavities, it returns the cumulative length of all such devices)
    """
    tfs = opt.optics.open(optics_filename)

    if 'b1' in tfs.header['sequence']:
        if flag_hllhc:
            layout = hllhc_b1_layout
        else:
            layout = lhc_b1_layout
    elif 'b2' in tfs.header['sequence']:
        if flag_hllhc:
            layout = hllhc_b2_layout
        else:
            layout = lhc_b2_layout
    else:
        raise ValueError('sequence must contain b1 or b2 in the name to indicate the beam')

    if flag_hllhc:
        s_ip1 = tfs.s[tfs.name == 'ip1'][0]
        s_ip5 = tfs.s[tfs.name == 'ip5'][0]
        s_ip8 = tfs.s[tfs.name == 'ip8'][0]

        # add longitudinal coordinates of DRF (IR1 & 5)

        # VAX - TAX before Q1
        s_drf_ir1 = np.array([17.5975, 18.8725, 21.2485])
        drf_length = 0.0322
        layout.sbeg['VAX'] = np.hstack((s_ip1 + s_drf_ir1, s_ip5 - s_drf_ir1, s_ip5 + s_drf_ir1,
                                        s_ip1 - s_drf_ir1 + tfs.header['length'])).tolist()
        layout.send['VAX'] = (np.array(layout.sbeg['VAX']) + drf_length).tolist()

        # DRF in triplets
        s_drf_ir1 = np.array([32.8955, 43.6805, 54.4655, 65.5655, 73.5195])
        drf_length = 0.1347
        layout.sbeg['DRFtrip'] = np.hstack((s_ip1 + s_drf_ir1, s_ip5 - s_drf_ir1, s_ip5 + s_drf_ir1,
                                            s_ip1 - s_drf_ir1 + tfs.header['length'])).tolist()
        layout.send['DRFtrip'] = (np.array(layout.sbeg['DRFtrip']) + drf_length).tolist()

        # DRF in D1 / DFXJ
        s_drf_ir1 = np.array([82.78356, 83.404444])
        drf_length = 0.1347
        layout.sbeg['DRFD1'] = np.hstack((s_ip1 + s_drf_ir1, s_ip5 - s_drf_ir1, s_ip5 + s_drf_ir1,
                                          s_ip1 - s_drf_ir1 + tfs.header['length'])).tolist()
        layout.send['DRFD1'] = (np.array(layout.sbeg['DRFD1']) + drf_length).tolist()

        # DRF in TAXN
        s_drf_ir1 = np.array([114.804356, 121.024356, 127.267846])
        drf_length = 0.1347
        layout.sbeg['DRFTAXN'] = np.hstack((s_ip1 + s_drf_ir1, s_ip5 - s_drf_ir1, s_ip5 + s_drf_ir1,
                                            s_ip1 - s_drf_ir1 + tfs.header['length'])).tolist()
        layout.send['DRFTAXN'] = (np.array(layout.sbeg['DRFTAXN']) + drf_length).tolist()

        # Y-chamber
        s_drf_ir1 = np.array([130.467])
        drf_length = 0.5
        layout.sbeg['YCHAMB'] = np.hstack((s_ip1 + s_drf_ir1, s_ip5 - s_drf_ir1, s_ip5 + s_drf_ir1,
                                           s_ip1 - s_drf_ir1 + tfs.header['length'])).tolist()
        layout.send['YCHAMB'] = (np.array(layout.sbeg['YCHAMB']) + drf_length).tolist()

        # DRF in crab cavities
        s_drf_ir1 = np.array([155.037651, 156.32575, 157.541856, 158.810651, 160.09875, 161.314856])
        drf_length = 0.227
        layout.sbeg['DRFCRAB'] = np.hstack((s_ip1 + s_drf_ir1, s_ip5 - s_drf_ir1, s_ip5 + s_drf_ir1,
                                            s_ip1 - s_drf_ir1 + tfs.header['length'])).tolist()
        layout.send['DRFCRAB'] = (np.array(layout.sbeg['DRFCRAB']) + drf_length).tolist()

        # some DRF between Q4 and Q6
        s_drf_ir1 = np.array([172.397621, 202.554621])
        drf_length = 0.1347
        layout.sbeg['DRFothers'] = np.hstack((s_ip1 + s_drf_ir1, s_ip5 - s_drf_ir1, s_ip5 + s_drf_ir1,
                                              s_ip1 - s_drf_ir1 + tfs.header['length'])).tolist()
        layout.send['DRFothers'] = (np.array(layout.sbeg['DRFothers']) + drf_length).tolist()

        # DRF in Q4 & Q5
        s_drf_ir1 = np.array([172.923918, 183.286657, 203.080918, 211.506657])
        drf_length = 0.1347
        layout.sbeg['DRFQ4Q5'] = np.hstack((s_ip1 + s_drf_ir1, s_ip5 - s_drf_ir1, s_ip5 + s_drf_ir1,
                                            s_ip1 - s_drf_ir1 + tfs.header['length'])).tolist()
        layout.send['DRFQ4Q5'] = (np.array(layout.sbeg['DRFQ4Q5']) + drf_length).tolist()

        layout.betas_lengths_names.extend(
            ['VAX', 'DRFtrip', 'DRFD1', 'DRFTAXN', 'YCHAMB', 'DRFCRAB', 'DRFothers', 'DRFQ4Q5'])

        # add longitudinal coordinates of VELO (IR8) - length is quite approximate (1m)
        # and we do not know on which side of the IP is most of the VELO (hence the
        # two lines below could be reversed with an opposite sign in front of 0.76... & 0.23...)
        # but I checked the impact on beta of such an inversion and it's negligible
        layout.sbeg['VELO'] = [s_ip8 - 0.76752]
        layout.send['VELO'] = [s_ip8 + 0.23248]
        layout.betas_lengths_names.append('VELO')

    all_s = tfs.s
    _, ind = np.unique(all_s, return_index=True)
    all_s = all_s[ind]
    all_betax = tfs.betx[ind]
    all_betay = tfs.bety[ind]
    # interpolating functions
    fx = interp1d(all_s, all_betax, kind='cubic')
    fy = interp1d(all_s, all_betay, kind='cubic')

    dict_betas_lengths = {}

    for name in layout.betas_lengths_names:

        length = 0
        betax = 0
        betay = 0

        for the_beg, the_end_ in zip(layout.sbeg[name], layout.send[name]):
            if the_end_ > tfs.header['length']:
                the_end = tfs.header['length']
            else:
                the_end = the_end_

            length += the_end - the_beg
            the_s = np.arange(the_beg, the_end_, 0.01)
            if the_end not in the_s:
                the_s = np.hstack((the_s, [the_end]))
            # average beta (through trapz integration)
            betax += np.trapz(fx(the_s), x=the_s)
            betay += np.trapz(fy(the_s), x=the_s)

        if length != 0:
            betax /= length
            betay /= length

        # divide by two the length for the MQWs (H & V) - the coord. are exactly the same without differentiating H and
        # V MQWs, we just assume they are located in a beta-symmetric way
        if name in ['MQW_H', 'MQW_V']:
            length /= 2
        if name == 'Warm_Pipe':
            length -= length_collimators

        dict_betas_lengths[name] = {
            'beta_x': betax,
            'beta_y': betay,
            'length': length
        }

    return dict_betas_lengths


def lhc_coll_settings_from_timber(timestamp: str, beam: str, twiss_file: Union[str, Path],
                                  new_coll_settings_filename: Union[str, Path], emittance: float,
                                  gamma: float = None, ref_coll_settings_filename: Union[str, Path] = None,
                                  print_progress: bool = False, timestamp_end: float = None, database: str = 'nxcals'):
    """
    Build and save a file with all collimator settings.
    :param timestamp: (local) time at which the settings will be taken from Timber,
        for instance '2018-04-02 12:54:00'
    :param beam: 1 or 2.
    :param twiss_file: full path of the Twiss file with the proper optics
        (used for coll. beta functions)
        collimator names, angles, materials, and thickness.
    :param new_coll_settings_filename: full path of the settings file to be
        produced (only if timestamp_end is None).
    :param emittance: transverse emittance (in m ).
    :param gamma: relativistic gamma factor.
    :param ref_coll_settings_filename: full path of the reference collimator settings file
        - should be a recent settings file. It's used to get all the
    :param print_progress: print some info on std output.
    :param timestamp_end: if specified, retrieve all collimator movements
        from timestamp until timestamp_end (otherwise take only the single timestamp
        'timestamp'). Returns all the collimator half-gaps  at these time
        (note: if the optics and/or gamma change during the time spanned, this won't
        be reflected in sigma, and hence in the number of sigmas,
        so these should NOT be used).
    :param database: 'nxcals' -> type of logging database to use.
    """
    if not pytimber_imported:
        raise ImportError('pytimber could not be imported')

    log = pytimber.LoggingDB(source=database)

    if ref_coll_settings_filename is None:
        ref_coll_settings_filename = os.path.join(data_directory, 'collimators',
                                                  f'lhc_collimators_reference_b{beam}.json')

    timber_var = 'MOTOR'  # more precise than LVDT except when it fails - but it failed only 2-3 times along
    # Run I/II (cf. Roderik Bruce)
    timber_var_tcdq = 'LVDT'  # MOTOR values for TCDQ are empty (at least in April 2018 and 2022)

    tfs = opt.optics.open(twiss_file)

    if gamma is None:
        gamma = tfs.header['gamma']

    # Here we specify the reference TCSP collimator
    # used to get the beam (orbit) offset at the TCDQ (which are one-jaw collimators).
    # On top of the orbit offset, there is also a mechanical offset
    # between TCSP and TCDQ, given here in mm
    # See Roderik Bruce, and Chiara Bracco for the mech. offset values (email of 10/04/2018)
    if beam == 1:
        tcsg_ref_name = "TCSP.A4R6.B1"
        # offsetmech_TCDQ = 0.19 # value for Run 2
        offset_mech_tcdq = 0.03  # value for Run 3 (see email D. Mirarchi to N. Mounet, 21/10/2022)
    elif beam == 2:
        tcsg_ref_name = "TCSP.A4L6.B2"
        # offsetmech_TCDQ = 0.09 # value for Run 2
        offset_mech_tcdq = 0.545 if int(gamma) == 479 else 0.545 - 0.2  # value for Run 3
        # (see email from D. Mirarchi to N. Mounet, 21/10/2022, and email from
        # C. Bracco to N. Mounet, 02/11/2022)
    else:
        raise ValueError("beam must be either 1 or 2")

    with open(ref_coll_settings_filename) as ref_coll_settings_file:
        ref_settings = json.load(ref_coll_settings_file)

    for name in ref_settings:
        if tcsg_ref_name in name:
            coll_ld = log.search(tcsg_ref_name + ":MEAS_{}_LD".format(timber_var))[0]
            coll_lu = log.search(tcsg_ref_name + ":MEAS_{}_LU".format(timber_var))[0]
            coll_rd = log.search(tcsg_ref_name + ":MEAS_{}_RD".format(timber_var))[0]
            coll_ru = log.search(tcsg_ref_name + ":MEAS_{}_RU".format(timber_var))[0]
            t_ldref, ldref = log.get(coll_ld, timestamp, t2=timestamp_end)[coll_ld]
            t_luref, luref = log.get(coll_lu, timestamp, t2=timestamp_end)[coll_lu]
            t_rdref, rdref = log.get(coll_rd, timestamp, t2=timestamp_end)[coll_rd]
            t_ruref, ruref = log.get(coll_ru, timestamp, t2=timestamp_end)[coll_ru]
            try:
                offsetref = (luref + ldref + ruref + rdref) / 4.
                t_offsetref = (t_luref + t_ldref + t_ruref + t_rdref) / 4.
            except ValueError:
                # case when arrays do not all have the same size (ldb)
                t_offsetref = sorted(np.hstack((t_ldref, t_rdref, t_luref, t_ruref)))
                offsetref = (np.interp(t_offsetref, t_ldref, ldref) + np.interp(t_offsetref, t_rdref, rdref) +
                             np.interp(t_offsetref, t_luref, luref) + np.interp(t_offsetref, t_ruref, ruref)) / 4.

            if print_progress:
                print("beam position at TCSP:", offsetref)
                print("total offset at TCDQ:", offsetref + offset_mech_tcdq)

    with open(ref_coll_settings_filename) as ref_coll_settings_file:
        ref_settings = json.load(ref_coll_settings_file)

    new_coll_settings_dict = {}
    for name, ref_coll_settings in ref_settings.items():
        print(name)

        new_coll_settings_dict[name] = ref_coll_settings.copy()

        phi = ref_coll_settings['angle']
        if 'TCDQ' in name.upper():
            coll_ld = log.search('TCDQA.%B' + str(beam) + ':MEAS_{}_LD'.format(timber_var_tcdq))[0]
            coll_lu = log.search('TCDQA.%B' + str(beam) + ':MEAS_{}_LU'.format(timber_var_tcdq))[0]
            beta_y = tfs.bety[tfs.name == name.lower()]
            beta_x = tfs.betx[tfs.name == name.lower()]

        else:
            coll_ld = log.search(name.upper() + '%MEAS_{}_LD'.format(timber_var))[0]
            coll_lu = log.search(name.upper() + '%MEAS_{}_LU'.format(timber_var))[0]
            coll_rd = log.search(name.upper() + '%MEAS_{}_RD'.format(timber_var))[0]
            coll_ru = log.search(name.upper() + '%MEAS_{}_RU'.format(timber_var))[0]
            beta_y = tfs.bety[tfs.name == name.lower()]
            beta_x = tfs.betx[tfs.name == name.lower()]

        data_ld = log.get(coll_ld, timestamp, t2=timestamp_end)
        data_lu = log.get(coll_lu, timestamp, t2=timestamp_end)
        t_coll_ld, data_coll_ld = data_ld[coll_ld]
        t_coll_lu, data_coll_lu = data_lu[coll_lu]
        data_coll_ld = np.squeeze(data_coll_ld)
        data_coll_lu = np.squeeze(data_coll_lu)
        t_coll_ld = np.squeeze(t_coll_ld)
        t_coll_lu = np.squeeze(t_coll_lu)

        new_halfgap = 0
        if 'TCDQ' not in name.upper():
            data_rd = log.get(coll_rd, timestamp, t2=timestamp_end)
            data_ru = log.get(coll_ru, timestamp, t2=timestamp_end)
            t_coll_rd, data_coll_rd = data_rd[coll_rd]
            t_coll_ru, data_coll_ru = data_ru[coll_ru]
            data_coll_rd = np.squeeze(data_coll_rd)
            data_coll_ru = np.squeeze(data_coll_ru)
            t_coll_rd = np.squeeze(t_coll_rd)
            t_coll_ru = np.squeeze(t_coll_ru)
            try:
                full_gap = (data_coll_ld - data_coll_rd +
                            data_coll_lu - data_coll_ru) / 2.  # average of the upstream and downstream gap
                t_gap = (t_coll_ld + t_coll_rd + t_coll_lu + t_coll_ru) / 4.
            except ValueError:
                # case when arrays do not all have the same size
                t_gap = sorted(np.hstack((t_coll_ld, t_coll_rd, t_coll_lu, t_coll_ru)))
                full_gap = (np.interp(t_gap, t_coll_ld, data_coll_ld) - np.interp(t_gap, t_coll_rd, data_coll_rd) +
                            np.interp(t_gap, t_coll_lu, data_coll_lu) - np.interp(t_gap, t_coll_ru, data_coll_ru)) / 2.

            new_halfgap = full_gap / 2. * 1e-3

        else:
            try:
                t_gap = (t_coll_ld + t_coll_lu) / 2.
                full_gap = ((data_coll_ld + data_coll_lu) / 2. -
                            (np.interp(t_gap, t_offsetref, offsetref) + offset_mech_tcdq))
            except ValueError:
                # case when arrays do not all have the same size
                t_gap = sorted(np.hstack((t_coll_ld, t_coll_lu)))
                full_gap = ((np.interp(t_gap, t_coll_ld, data_coll_ld) + np.interp(t_gap, t_coll_lu,
                                                                                   data_coll_lu)) / 2 -
                            (np.interp(t_gap, t_offsetref, offsetref) + offset_mech_tcdq))

            new_halfgap = full_gap * 1e-3

        new_coll_settings_dict[name]['halfgap'] = new_halfgap

        beta_x = beta_x[0]
        beta_y = beta_y[0]
        new_coll_settings_dict[name]['beta_x'] = round(beta_x, 10)
        new_coll_settings_dict[name]['beta_y'] = round(beta_y, 10)
        sigma = np.sqrt(emittance / gamma * beta_x * np.cos(phi) ** 2 + emittance / gamma * beta_y * np.sin(phi) ** 2)
        n_sigma = new_halfgap / sigma
        new_coll_settings_dict[name]['n_sigma'] = n_sigma

        if timestamp_end is not None:
            new_coll_settings_dict[name]['timestamp'] = [datetime.datetime.fromtimestamp(t) for t in t_gap]

        if print_progress and timestamp_end is None:
            print(name, str(new_halfgap) + ' m', str(beta_x) + ' m', str(beta_y) + ' m')

    if timestamp_end is None:
        with open(new_coll_settings_filename, "w") as output_file:
            json.dump(new_coll_settings_dict, output_file, indent=2)
        return new_coll_settings_dict
    else:
        return new_coll_settings_dict


def compute_detuning_coefficients_from_optics(twiss_file: Union[str, Path], save_file=True,
                                              output_file: Union[str, Path] = 'octupole.dat',
                                              momentum: float = 7000e9, o3_max: float = 63100) -> np.ndarray:
    """
    Compute the detuning coefficients from a twiss file
    :param twiss_file: the twiss file containing the octupole information
    :param save_file: save the computed coefficients to a file
    :param output_file: the name of the file where the coefficients are saved
    :param momentum: the beam momentum
    :param o3_max: maximum absolute octupolar strength in T/m^3 (default value from MAD-X)
    """
    # o3_max is maximum absolute octupolar strength in T/m^3 (from MAD-X)

    # compute optics at octupoles
    tfs = opt.optics.open(twiss_file)

    flag_octupole = [name.startswith('mo') for name in tfs.name]

    out_table = np.vstack((tfs.name[flag_octupole], tfs.s[flag_octupole], tfs.l[flag_octupole], tfs.betx[flag_octupole],
                           tfs.bety[flag_octupole], tfs.dx[flag_octupole])).T

    if save_file:
        np.savetxt(output_file, out_table, header='name s l betx bety dx', fmt='%s')

    # compute detuning coefficients
    k3 = 6 * o3_max / (momentum / c)  # k3+ (maximum normalized octupolar strength)

    length = np.array([float(oct_length) for oct_length in tfs.l[flag_octupole]])
    beta_x = np.array([float(betx) for betx in tfs.betx[flag_octupole]])
    beta_y = np.array([float(bety) for bety in tfs.bety[flag_octupole]])
    dx = np.array([float(dx) for dx in tfs.dx[flag_octupole]])

    # ind_f = np.where(beta_x > (beta_x.max() * 0.5)) # focuses in X plane means high beta_x - WRONG with TeleIndex!
    # ind_d = np.where(beta_y > (beta_y.max() * 0.5)) # defocuses in X plane means high beta_y - WRONG with TeleIndex!
    # (see S. Fartoukh)
    ind_f = np.where(dx > 1.5)  # focuses in X plane means dispersion~=2
    ind_d = np.where(dx <= 1.5)  # defocuses in X plane means dispersion~=1i (see S. Fartoukh)

    # not: additional minus sign for the defocusing octupoles because O3D=-O3F for
    # the same current in foc. and defoc. octupoles

    ax_f = np.sum(length[ind_f] * beta_x[ind_f] ** 2) * k3 / (16 * np.pi)
    ax_d = -np.sum(length[ind_d] * beta_x[ind_d] ** 2) * k3 / (16 * np.pi)
    ay_f = np.sum(length[ind_f] * beta_y[ind_f] ** 2) * k3 / (16 * np.pi)
    ay_d = -np.sum(length[ind_d] * beta_y[ind_d] ** 2) * k3 / (16 * np.pi)
    axy_f = -np.sum(length[ind_f] * beta_x[ind_f] * beta_y[ind_f]) * k3 / (8 * np.pi)
    axy_d = np.sum(length[ind_d] * beta_x[ind_d] * beta_y[ind_d]) * k3 / (8 * np.pi)

    coeff_oct = np.array([ax_f, ax_d, ay_f, ay_d, axy_f, axy_d])

    if save_file:
        np.savetxt('coeff_oct.dat', coeff_oct)

    return coeff_oct


def compute_normalized_detuning_coefficients(detuning_coeffs: np.ndarray,
                                             gamma: float, i_focusing_oct: float,
                                             i_defocusing_oct: float, epsnormx: float,
                                             epsnormy: float) -> Tuple[float, float, float, float]:
    """
    Computes the detuning coefficients bx, bxy, byx, by normalized by the emittance (i.e. with the convention needed for
    the stability diagram theory)
    :param detuning_coeffs: the detuning coeffiecients (computed with compute_detuning_coefficients_from_optics)
    :param gamma: relativistic gamma
    :param i_focusing_oct: octupole current in the focusing octupoles (A)
    :param i_defocusing_oct: octupole current in the defocusing octupoles (A)
    :param epsnormx: normalized emittance in x plane in meters
    :param epsnormy: normalized emittance in y plane in meters
    :return: the normalized detuning coefficients bx, bxy, byx, by
    """
    beta_rel = np.sqrt(1 - 1 / (gamma ** 2))
    gamma_ref = 7460.52
    i_oct_ref = 550
    i_focusing_oct_norm = i_focusing_oct / i_oct_ref * (gamma_ref / gamma)
    i_defocusing_oct_norm = i_defocusing_oct / i_oct_ref * (gamma_ref / gamma)
    eps1sigx = epsnormx / (beta_rel * gamma)
    eps1sigy = epsnormy / (beta_rel * gamma)
    bx = detuning_coeffs[0] * i_focusing_oct_norm * eps1sigx + detuning_coeffs[1] * i_defocusing_oct_norm * eps1sigx
    bxy = detuning_coeffs[4] * i_focusing_oct_norm * eps1sigy + detuning_coeffs[5] * i_defocusing_oct_norm * eps1sigy
    by = detuning_coeffs[2] * i_focusing_oct_norm * eps1sigy + detuning_coeffs[3] * i_defocusing_oct_norm * eps1sigy
    byx = detuning_coeffs[4] * i_focusing_oct_norm * eps1sigx + detuning_coeffs[5] * i_defocusing_oct_norm * eps1sigx

    return bx, bxy, byx, by


def compute_octupole_current_from_detuning_coeffs(b_direct: float, b_cross: float, epsnormx: float, epsnormy: float,
                                                  gamma: float, detuning_coeffs: np.ndarray, plane: str,
                                                  i_foc_ref: float = 550, i_def_ref: float = -550,
                                                  gamma_ref: float = 7460.52) -> Tuple[float, float]:
    beta_rel = np.sqrt(1 - 1 / (gamma ** 2))

    eps1sigmax = epsnormx / (beta_rel * gamma)
    eps1sigmay = epsnormy / (beta_rel * gamma)

    if plane == 'x':
        a = detuning_coeffs[0] * eps1sigmax
        b = detuning_coeffs[1] * eps1sigmax
        c = detuning_coeffs[4] * eps1sigmay
        d = detuning_coeffs[5] * eps1sigmay
    elif plane == 'y':
        a = detuning_coeffs[2] * eps1sigmay
        b = detuning_coeffs[3] * eps1sigmay
        c = detuning_coeffs[4] * eps1sigmax
        d = detuning_coeffs[5] * eps1sigmax
    else:
        raise ValueError('plane must be either x or y')

    mat = np.array([[a, b], [c, d]])

    i_foc_norm, i_def_norm = np.dot(np.linalg.inv(mat), np.array([b_direct, b_cross]))

    i_foc = i_foc_norm * i_foc_ref / (gamma_ref / gamma)
    i_def = i_def_norm * i_def_ref / (gamma_ref / gamma)

    return i_foc, i_def
