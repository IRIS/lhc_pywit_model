from __future__ import annotations

import os.path

import numpy as np
import inspect

from lhc_pywit_model.utils import execute_jobs_on_htcondor
from lhc_pywit_model.utils import multiply_element_by_frequency_dependent_function, create_layer_from_dict
from lhc_pywit_model.package_paths import data_directory

from pywit.element import Element

from pywit.elements_group import ElementsGroup
from pywit.interface import (import_data_iw2d, _create_iw2d_input_from_dict,
                             create_component_from_data, get_iw2d_config_value,
                             RoundIW2DInput, check_already_computed,
                             add_iw2d_input_to_database)
from pywit.utilities import create_resonator_element
from pywit.component import Component

from typing import List, Union, Callable, Tuple, Dict
from pathlib import Path

import json

from scipy.constants import c, mu_0

z_0 = mu_0 * c


class EllipticElementsGroup(ElementsGroup):
    """
    A group of collimators. It is used to group several sections of the beam pip with similar features. Each section is
    stored as an element.
    """

    def __init__(self, lxplusbatch: str, parameters_filename: Union[str, Path],
                 betas_lengths_dict: Dict[str, Dict[str, float]], f_params_dict: dict, z_params_dict: dict,
                 relativistic_gamma: float, machine: str,
                 b_field: float = 0, compute_wake: bool = False,
                 power_loss_param: list = None,
                 yokoya_factors_filename: Union[str, Path] = os.path.join(data_directory, 'elliptic_elements',
                                                                          'Yokoya_factors_elliptic.dat'),
                 jobs_submission_function: Callable = execute_jobs_on_htcondor, name: str = '',
                 pumping_holes_parameters_filename: Union[str, Path] = None,
                 pumping_holes_model: str = 'Mostacci', f_cutoff: float = 50e9, additional_f_params: dict = None):
        r"""
        The initialization function for the EllipticElementsGroup class.
        :param lxplusbatch: a string which is "launch" if IW2D calculations should be launched or "retrieve" if the
        results should be gathered
        :param parameters_filename: the path to a json file encoding a dict, the keys of which being the name
        of each elliptic element and the values a dict with its parameters as follows:
            - small_axis: the small axis of the ellipse
            - large_axis: the large axis of the ellipse
            - orientation: the orientation of the ellipse, i.e. the orientation of the large axis
            - layers: a list of dictionaries with the following parameters for each layer of the element:
                * material: the material of the layer (the material must be present either in materials.py, in
                data/materials.json or in the PyWIT
                materials library)
                * thickness: the thickness of the coating (in m)
            - frequency_dependent_factor_file: the path to a file specifying a frequency dependent factor for each
              component of the impedance. This file can either be the name of one of the files in
              lhc_pywit_model/data/beam_screen_weld or any user-specifed file (in this case the absolute path of the
              file must be specified). It contains a table indicating a frequency-dependent scalar by which the
              impedance is multiplied
              (columns of the table: Freq[Hz] Zlong_Re Zlong_Im Zxdip_Re Zxdip_Im Zydip_Re Zydip_Im Zxquad_Re Zxquad_Im
              Zyquad_Re Zyquad_Im)
        :param betas_lengths_dict: A dictionary associating to each elliptic element a dictionary containing the element
                                   length, the beta_x and beta_y functions and the length of the element
        :param f_params_dict: a dictionary specifying the frequency sampling parameters (as in pywit.interface.Sampling)
        :param z_params_dict: a dictionary specifying the distance sampling parameters (as in pywit.interface.Sampling)
        :param relativistic_gamma: the relativistic $\gamma$ factor of the beam
        :param machine: the machine name
        :param b_field: b-field in the elements (used to take into account magnetoresistance in copper)
        :param compute_wake: a bool specifying if the wakes should be computed too
        :param power_loss_param: parameters used in the power loss computation (NOT IMPLEMENTED YET)
        :param name: the name of this group
        :param yokoya_factors_filename: a file with a table containing the Yokoya factors for several aspect ratios
        (columns: x (the aspect ratio), dipy, dipx, quady, quadx)
        :param jobs_submission_function: a function which handles the submission of IW2D jobs. The function must take
        the following parameters: jobs_directory, sub_file_name
        :param pumping_holes_parameters_filename: a table with the pumping holes parameters (columns of the table:
        name, Lh, Wh, T, b, d, eta, rhob, rhod, length, nb_holes_per_cs)
        :param pumping_holes_model: a string specifying the pumping holes model. For now it can only be 'Mostacci'
        :param f_cutoff: the cutoff frequency (used for the broadband model)
        """
        self.f_cutoff = f_cutoff
        self.parameters_filename = parameters_filename
        self.f_params_dict = f_params_dict
        self.additional_f_params = additional_f_params
        self.z_params_dict = z_params_dict
        self.b_field = b_field
        self.compute_wake = compute_wake
        self.power_loss_param = power_loss_param
        self.lxplusbatch = lxplusbatch
        self.relativistic_gamma = relativistic_gamma
        self.machine = machine

        self.name = name
        self.add_pumping_holes = pumping_holes_parameters_filename is not None
        self.pumping_holes_parameters_filename = pumping_holes_parameters_filename
        self.pumping_holes_model = pumping_holes_model

        # read the yokoya factors
        self.yokoya_factors_filename = yokoya_factors_filename
        self.yokoya_factors_table = np.loadtxt(yokoya_factors_filename, skiprows=1)

        with open(parameters_filename, 'r') as parameters_file:
            self.parameters = json.load(parameters_file)

        self.names = list(self.parameters.keys())

        # read betas and lengths
        self.beta_length_dict = betas_lengths_dict

        self.elements_list = self.create_elements_list()

        self.jobs_submission_function = jobs_submission_function

        self.iw2d_inputs_list = self.create_elliptic_elements_iw2d_inputs_list(machine=machine,
                                                                               calculate_wake=compute_wake,
                                                                               relativistic_gamma=relativistic_gamma)

        self.launch_or_retrieve()

        super().__init__(elements_list=self.elements_list, name=self.name)

    def create_elements_list(self) -> List[Element]:
        elements_list = []

        for ind_name, name in enumerate(self.names):
            elements_list.append(Element(length=self.beta_length_dict[name]['length'],
                                         beta_x=self.beta_length_dict[name]['beta_x'],
                                         beta_y=self.beta_length_dict[name]['beta_y'],
                                         name=name,
                                         ))

        return elements_list

    def create_elliptic_elements_iw2d_inputs_list(self, machine: str, calculate_wake: bool,
                                                  relativistic_gamma: float) -> list[RoundIW2DInput]:
        list_iw2d_inputs = []

        if calculate_wake:
            calculate_wake_str = 'true'
        else:
            calculate_wake_str = 'false'

        for ind_name, name in enumerate(self.names):
            element_parameters = self.parameters[name]
            aspect_ratio = ((element_parameters['large_axis'] - element_parameters['small_axis']) /
                            (element_parameters['large_axis'] + element_parameters['small_axis']))

            # for every element find the layers and construct a dictionary of inputs for IW2D
            dict_elem = {'comment': '',
                         'machine': machine,
                         'is_round': 'true',
                         'layers': [],
                         'calculate_wake': calculate_wake_str,
                         'length': self.beta_length_dict[name]['length'],
                         'yokoya_factors': self.get_yokoya_factors(aspect_ratio, element_parameters['orientation']),
                         'relativistic_gamma': relativistic_gamma,
                         'inner_layer_radius': element_parameters['small_axis'],
                         'f_params': self.f_params_dict.copy(),
                         'z_params': self.z_params_dict.copy()
                         }

            if self.additional_f_params is not None:
                for k in self.additional_f_params.keys():
                    dict_elem[k] = self.additional_f_params[k]

            for layer_dict in element_parameters['layers']:
                dict_elem['layers'].append(create_layer_from_dict(layer_dict, b_field=self.b_field).__dict__)

            list_iw2d_inputs.append(_create_iw2d_input_from_dict(d=dict_elem))

        return list_iw2d_inputs

    def get_yokoya_factors(self, aspect_ratio, orientation):
        if aspect_ratio == 0:
            # round case (the aspect ratio definition is (large_axis - small_axis)/(large_axis + small_axis))
            return '1 1 1 0 0'

        if orientation == 'v':
            return (str(1) + ' ' +
                    str(np.interp(aspect_ratio, self.yokoya_factors_table[:, 0],
                                  self.yokoya_factors_table[:, 2])) + ' ' +
                    str(np.interp(aspect_ratio, self.yokoya_factors_table[:, 0],
                                  self.yokoya_factors_table[:, 1])) + ' ' +
                    str(np.interp(aspect_ratio, self.yokoya_factors_table[:, 0],
                                  self.yokoya_factors_table[:, 4])) + ' ' +
                    str(np.interp(aspect_ratio, self.yokoya_factors_table[:, 0], self.yokoya_factors_table[:, 3])))
        elif orientation == 'h':
            return (str(1) + ' ' +
                    str(np.interp(aspect_ratio, self.yokoya_factors_table[:, 0],
                                  self.yokoya_factors_table[:, 1])) + ' ' +
                    str(np.interp(aspect_ratio, self.yokoya_factors_table[:, 0],
                                  self.yokoya_factors_table[:, 2])) + ' ' +
                    str(np.interp(aspect_ratio, self.yokoya_factors_table[:, 0],
                                  self.yokoya_factors_table[:, 3])) + ' ' +
                    str(np.interp(aspect_ratio, self.yokoya_factors_table[:, 0], self.yokoya_factors_table[:, 4])))
        else:
            raise ValueError("The orientation of elliptic element must be h for horizontal or v for vertical")

    def launch_rw_simulations(self):
        projects_path = Path(get_iw2d_config_value('project_directory'))
        bin_path = Path(get_iw2d_config_value('binary_directory'))
        hashes_dict = {}

        # Here we create the condor input files and launch the simulations
        # Create a folder for each element
        for name, iw2d_input in zip(self.names, self.iw2d_inputs_list):
            already_computed, input_hash, elem_dir = check_already_computed(iw2d_input, name)
            hashes_dict[name] = {'input_hash': input_hash}

            if already_computed:
                hashes_dict[name]['already_computed'] = True
                continue
            else:
                hashes_dict[name]['already_computed'] = False

            add_iw2d_input_to_database(iw2d_input, input_hash, elem_dir)

            executable_file_name = os.path.join(elem_dir, 'iw2d_job.txt')

            executable = 'roundchamber.x'
            if self.compute_wake:
                executable = "wake_" + executable

            text = f"#!/bin/bash\n" \
                   f"{bin_path}/{executable} < {elem_dir}/input.txt\n"

            with open(executable_file_name, 'w') as executable_file:
                executable_file.write(text)

        # Create the job sub file
        jobs_submission_function_args = inspect.getfullargspec(self.jobs_submission_function).args
        if set(jobs_submission_function_args) != {'hashes_dict', 'sub_file_name'}:
            raise TypeError('Job submission function must take the arguments hashes_dict, '
                            'sub_file_name')
        sub_file_name = os.path.join(projects_path, 'elliptic_elements.sub')
        self.jobs_submission_function(hashes_dict=hashes_dict,
                                      sub_file_name=sub_file_name)

    def retrieve_rw_simulations(self):
        if self.add_pumping_holes:
            with open(self.pumping_holes_parameters_filename, mode='r') as pumping_holes_parameters_file:
                pumping_holes_parameters_dict = json.load(pumping_holes_parameters_file)

        for ielem, element in enumerate(self.elements_list):
            already_computed, _, element_dir = check_already_computed(self.iw2d_inputs_list[ielem], self.name)
            if not already_computed:
                raise ValueError(f'Element {self.name} not computed')

            data_iw2d_list = import_data_iw2d(directory=element_dir, common_string='')
            components_list = []
            for data_iw2d in data_iw2d_list:
                components_list.append(create_component_from_data(*data_iw2d,
                                                                  relativistic_gamma=self.relativistic_gamma))

            elem_name = element.name
            elem_length = element.length
            element += Element(length=element.length,
                               beta_x=element.beta_x,
                               beta_y=element.beta_y,
                               name=element.name,
                               components=components_list
                               )
            element.name = elem_name
            element.length = elem_length

            self.elements_list[ielem] = element

            if ('frequency_dependent_factor_file' in self.parameters[elem_name].keys() and
                    self.parameters[elem_name]['frequency_dependent_factor_file'] is not None):

                # if it exists take the file from lhc_pywit_model/data, otherwise use it as an absolute path
                frequency_dependent_factor_file = os.path.join(
                    data_directory, 'beam_screen_weld',
                    self.parameters[elem_name]['frequency_dependent_factor_file']
                )

                if not os.path.exists(frequency_dependent_factor_file):
                    frequency_dependent_factor_file = self.parameters[elem_name]['frequency_dependent_factor_file']

                freq_dep_factor_table = np.loadtxt(frequency_dependent_factor_file, skiprows=1)

                scalar_functions_real_list = []
                scalar_functions_imag_list = []
                test_exponents_list = []
                source_exponents_list = []
                # long
                scalar_functions_real_list.append(freq_dep_factor_table[:, 1])
                scalar_functions_imag_list.append(freq_dep_factor_table[:, 2])
                test_exponents_list.append((0, 0))
                source_exponents_list.append((0, 0))
                # xdip
                scalar_functions_real_list.append(freq_dep_factor_table[:, 3])
                scalar_functions_imag_list.append(freq_dep_factor_table[:, 4])
                source_exponents_list.append((1, 0))
                test_exponents_list.append((0, 0))
                # ydip
                scalar_functions_real_list.append(freq_dep_factor_table[:, 5])
                scalar_functions_imag_list.append(freq_dep_factor_table[:, 6])
                source_exponents_list.append((0, 1))
                test_exponents_list.append((0, 0))
                # xquad
                scalar_functions_real_list.append(freq_dep_factor_table[:, 7])
                scalar_functions_imag_list.append(freq_dep_factor_table[:, 8])
                source_exponents_list.append((0, 0))
                test_exponents_list.append((1, 0))
                # yquad
                scalar_functions_real_list.append(freq_dep_factor_table[:, 9])
                scalar_functions_imag_list.append(freq_dep_factor_table[:, 10])
                source_exponents_list.append((0, 0))
                test_exponents_list.append((0, 1))

                new_element = multiply_element_by_frequency_dependent_function(
                    element=element,
                    scalar_functions_real_list=scalar_functions_real_list,
                    scalar_functions_imag_list=scalar_functions_imag_list,
                    test_exponents_list=test_exponents_list,
                    source_exponents_list=source_exponents_list,
                    frequencies=freq_dep_factor_table[:, 0])

                self.elements_list[ielem] = new_element

        for ielem, element in enumerate(self.elements_list):
            if self.add_pumping_holes:
                if self.pumping_holes_model == 'mostacci':
                    section_dict = pumping_holes_parameters_dict[element.name]
                    number_of_devices = self.beta_length_dict[element.name]['length'] / section_dict['length']
                    self.elements_list[ielem] += MostacciPumpingHolesElement(
                        lh=section_dict['Lh'], wh=section_dict['Wh'], t=section_dict['T'], b=section_dict['b'],
                        d=section_dict['d'], eta=section_dict['eta'], rhob=section_dict['rhob'],
                        rhod=section_dict['rhod'],
                        number_of_holes_per_crosssection=section_dict['nb_holes_per_cs'],
                        length=section_dict['length'], cm=1, ce=1, f_cutoff=self.f_cutoff,
                        beta_x=self.beta_length_dict[element.name]['beta_x'],
                        beta_y=self.beta_length_dict[element.name]['beta_y'], name=element.name
                    ).element * number_of_devices
                    self.elements_list[ielem].length = self.beta_length_dict[element.name]['length']
                    self.elements_list[ielem].beta_x = self.beta_length_dict[element.name]['beta_x']
                    self.elements_list[ielem].beta_y = self.beta_length_dict[element.name]['beta_y']
                    self.elements_list[ielem].name = element.name
                else:
                    raise ValueError('Only Mostacci model is implemented')

        self.components = sum(self.elements_list).components

        if not self.compute_wake:
            for component in self.components:
                component.wake = lambda x: 0

    def launch_or_retrieve(self):
        if self.lxplusbatch in ['launch', None]:
            self.launch_rw_simulations()
        if self.lxplusbatch in ['retrieve', None]:
            self.retrieve_rw_simulations()


class MostacciPumpingHolesElement:
    def __init__(self, lh: float, wh: float, t: float, b: float, d: float, eta: float, rhob: float, rhod: float,
                 length: float, f_cutoff: float, number_of_holes_per_crosssection: float, cm: float, ce: float,
                 beta_x: float, beta_y: float, name: str = ''):
        self.name = name
        self.lh = lh
        self.wh = wh
        self.t = t
        self.b = b
        self.d = d
        self.eta = eta
        self.rhob = rhob
        self.rhod = rhod
        self.length = length
        self.f_cutoff = f_cutoff
        self.omega_cutoff = 2 * np.pi * self.f_cutoff
        self.number_of_holes_per_crosssection = number_of_holes_per_crosssection
        self.ce = ce
        self.cm = cm
        self.beta_x = beta_x
        self.beta_y = beta_y

        self.area = wh * (lh - wh) + np.pi * wh ** 2 / 4
        self.b2 = self.b + t

        self.a = (np.sqrt(rhob) / self.b2 + np.sqrt(rhod) / d) * np.sqrt(mu_0 / 2) / (2 * z_0 * np.log(d / self.b2))

        self.n = eta * 2 * np.pi * self.b2 * length / self.area
        self.d = number_of_holes_per_crosssection * self.area / (2 * np.pi * self.b2 * eta)

        self.alphae, self.alpham = self.polarizabilities_holes(self.t)
        self.alphae_0, self.alpham_0 = self.polarizabilities_holes(0)

        self.longitudinal_rs_kurennoy = self.omega_cutoff * mu_0 * eta * ((self.alphae_0 + self.alpham_0) /
                                                                          (2 * np.pi * self.area * b))
        self.transverse_rs_kurennoy = 2 * z_0 * eta * (self.alphae_0 + self.alpham_0) / (np.pi * self.area * b ** 3)

        # the model is a broadband resonator in which the longitudinal impedance is modified by an additional
        # component, while the wake is untouched

        rs = {
            'z0000': self.longitudinal_rs_kurennoy * self.length,
            'x1000': self.transverse_rs_kurennoy * self.length,
            'y0100': self.transverse_rs_kurennoy * self.length,
        }

        fs = {
            'z0000': self.f_cutoff,
            'x1000': self.f_cutoff,
            'y0100': self.f_cutoff,
        }

        qs = {
            'z0000': 1,
            'x1000': 1,
            'y0100': 1,
        }

        self.element = create_resonator_element(length=self.length, beta_x=self.beta_x, beta_y=self.beta_y, rs=rs,
                                                qs=qs, fs=fs)

        self.element += Element(length=self.length, beta_x=self.beta_x, beta_y=self.beta_y,
                                components=[Component(impedance=lambda x: self.longitudinal_mostacci_component(x),
                                                      wake=lambda x: 0, plane='z', source_exponents=(0, 0),
                                                      test_exponents=(0, 0))], name=self.name)

    def longitudinal_mostacci_component(self, freq: np.typing.NDArray) -> np.typing.NDArray:
        z_long = np.zeros_like(freq)

        omega = 2 * np.pi * freq
        alpha = self.a * np.sqrt(omega)

        inds_below_cutoff = np.where(freq <= self.f_cutoff)[0]
        z_long[inds_below_cutoff] = (z_0 * omega[inds_below_cutoff] ** 2 *
                                     (self.alpham + self.alphae) ** 2 *
                                     (self.n / 2 + self.n / (alpha[inds_below_cutoff] * self.d) +
                                      (np.exp(-alpha[inds_below_cutoff] * self.d * self.n) - 1) /
                                      (alpha[inds_below_cutoff] * self.d) ** 2) /
                                     (16 * np.pi ** 3 * self.b ** 2 * self.b2 ** 2 * np.log(self.b / self.b2) * c ** 2))

        return z_long

    def polarizabilities_holes(self, t) -> Tuple[int, int]:
        lr = self.lh - (1 - np.pi / 4) * self.wh

        x = self.wh / self.lh
        v = self.lh * self.wh ** 2

        alphae = -self.ce * v * np.pi / 16 * (1 - 0.765 * x + 0.1894 * x ** 2) * np.exp(
            -np.pi * t * np.sqrt(1 / lr ** 2 + 1 / self.wh ** 2))
        alpham = self.cm * v * np.pi / 16 * (1 - 0.0857 * x - 0.0654 * x ** 2) * np.exp(-np.pi * t / self.wh)

        return alphae, alpham
