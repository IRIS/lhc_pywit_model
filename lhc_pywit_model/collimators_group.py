from __future__ import annotations

import os.path

import json
import inspect

from pywit.elements_group import ElementsGroup
from lhc_pywit_model.utils import execute_jobs_on_htcondor
from lhc_pywit_model.collimator import Collimator, halfgap_dependent_hom
from lhc_pywit_model.parameters import DEFAULT_RESONATOR_F_ROI_LEVEL

from pywit.interface import (check_already_computed, add_iw2d_input_to_database,
                             get_iw2d_config_value)

from typing import List, Union, Callable
from pathlib import Path

import pyoptics

import numpy as np


class CollimatorsGroup(ElementsGroup):
    """
    A group of collimators. Each collimator is stored as an element.
    """

    def __init__(self, settings_filename: Union[str, Path], lxplusbatch: str, optics_filename: Union[Path, str],
                 relativistic_gamma: float,
                 f_params_dict: dict, z_params_dict: dict,
                 compute_geometric_impedance: bool, compute_taper_RW_impedance: bool,
                 use_single_layer_approx_for_coated_taper: bool, compute_wake: bool,
                 taper_settings: Union[str, Path], jobs_submission_function: Callable = execute_jobs_on_htcondor,
                 normalized_emittance: float = None, machine: str = 'LHC',
                 name: str = '', f_cutoff_bb: float = 50e9,
                 additional_f_params: dict = None, resonator_f_roi_level: float = DEFAULT_RESONATOR_F_ROI_LEVEL,
                 frequency_parameters_for_taper_rw: dict = None):
        r"""
        The initialization function for the CollimatorsGroup class.
        :param settings_filename: the path to a json file each item of which stores the parameters of a collimators as
        follows:
            - angle: the angle indicating the orientation of a collimator (e.g. it is 0 for a horizontal collimator and
                     $\frac{\pi}{2}$ for a vertical collimator)
            - layers: a list of dictionaries indicating with the following parameters for each layer of the collimator:
                * material: the material of the layer (the material must be present either in the json or in the python
                in the materials library)
                * thickness: the thickness of the coating
            - halfgap: the halfgap of the collimator in meters
            - length: the length of the collimator
            - taper_type: a keyword indicating the geometry of the taper. It can be anything defined in taper_settings
                (see below)
            - halfgap_dependent_hom_files: a list of files containing the parameters of an half-gap dependent
            high-order mode. The file must be located in the `lhc_pywit_model/data` folder and it must contain a
            table in which the columns indicate: halfgap, x shunt impedance [Ohm/m], y shunt impedance [Ohm/m], x Q
            factor, y Q factor, x frequency and y frequency (all impedances are dipolar). An example of such a file is
            `lhc_pywit_model/data/collimators/TCTPmode_vs_fullgap.txt`
            - asymmetry_factor: (optional) a factor that specifies if the collimator is asymmetric. Values:
                * 1 for symmetric collimator
                * 0 for single-jaw collimator
                * otherwise it is a factor that multiplies the top half-gap to get the bottom one
        :param lxplusbatch:  a string which is "launch" if IW2D calculations should be launched or "retrieve" if the
        results should be gathered, or None if computation should be launched and retrieved locally.
        :param optics_filename: the path to a twiss file with the optics
        :param relativistic_gamma: the relativistic $\gamma$ factor of the beam
        :param f_params_dict: a dictionary specifying the frequency sampling parameters (as in pywit.interface.Sampling)
        :param z_params_dict: a dictionary specifying the distance sampling parameters (as in pywit.interface.Sampling)
        :param compute_geometric_impedance: a bool specifying if the geometric impedance should be computed
        :param compute_taper_RW_impedance: a bool specifying if the taper RW impedance should be computed
        :param use_single_layer_approx_for_coated_taper: a bool specifying if we use only a single layer for
        coated tapers (RW part) - otherwise calculations are much slower (and overall impact is small)
        :param compute_wake: a bool specifying if the wakes should be computed too
        :param taper_settings: the path to a json file specifying the taper parameters. The contains for each taper type
        a dictionary specifying list of tuples containing the length, the width and the delta of the taper (which is the
        difference in aperture between the larger and narrower ends of the taper), as well
        as the materials in which the taper is made (for the RW part), as follows:
        ...
        'taper_type':
          [
            {"length": ..., "delta_halfgap": ..., "width": ...,
             "layers": [{"material": ..., "thickness": ...}, {"material": ..., "thickness": ...}],
             },
            {"length": ..., "delta_halfgap": ..., "width": ...,
             "layers": [{"material": ..., "thickness": ...}],
             }
          ]
        ...
        Flat parts (with zero slope, i.e. showing no geometric impedance but only
        RW impedance), can be included by putting delta_halfgap=0.
        :param jobs_submission_function: a function which handles the submission of IW2D jobs. Function must take the
        following parameters: jobs_directory, collimators_folders_list, sub_file_name, submit_jobs
        :param normalized_emittance: the normalized emittance in microns (used for the computation of collimator
        half-gaps when they are input as a number of sigmas)
        :param machine: the machine name
        :param name: the name of this group
        :param f_cutoff_bb: The cutoff frequency used for the broad-band impedances
        :param additional_f_params: additional parameters (typically, for frequencies scan) to be added to IW2D input
        :param frequency_parameters_for_taper_rw: a dictionary of parameters used in the interpolation of the taper
        resistive wall component impedance function. The dictionary must contain the following items:
            * freq_start: the lowest frequency of the interpolation
            * freq_stop: the highest frequency of the interpolation
            * num_points: the number of interpolation points
        The interpolation frequencies are then calculated as np.geomspace(freq_start, freq_stop, num_points)
        """
        self.settings_filename = settings_filename
        self.compute_wake = compute_wake
        self.jobs_submission_function = jobs_submission_function
        self.relativistic_gamma = relativistic_gamma
        self.f_cutoff_bb = f_cutoff_bb
        self.name = name
        self.lxplusbatch = lxplusbatch
        self.machine = machine
        self.normalized_emittance = normalized_emittance
        self.optics_filename = optics_filename
        self.compute_geometric_impedance = compute_geometric_impedance
        self.compute_taper_RW_impedance = compute_taper_RW_impedance
        self.use_single_layer_approx_for_coated_taper = use_single_layer_approx_for_coated_taper
        self.taper_settings = taper_settings
        self.frequency_parameters_for_taper_rw = frequency_parameters_for_taper_rw

        with open(self.settings_filename, 'r') as settings_file:
            self.settings_dict = json.load(settings_file)

        with open(taper_settings, 'r') as taper_settings_file:
            self.taper_data = json.load(taper_settings_file)

        self.names = list(self.settings_dict.keys())

        self.f_params_dict = f_params_dict
        self.z_params_dict = z_params_dict
        self.additional_f_params = additional_f_params

        self.resonator_f_roi_level = resonator_f_roi_level

        # Directory containing the jobs inputs for all the collimators

        self.elements_list = self.create_elements_list()

        self.init_collimators_iw2d_inputs()

        self.launch_or_retrieve()

        super().__init__(elements_list=self.elements_list, name=self.name)

    def init_collimators_iw2d_inputs(self):
        if self.compute_wake:
            compute_wake_str = 'true'
        else:
            compute_wake_str = 'false'

        for collimator in self.elements_list:
            collimator.init_iw2d_input(machine=self.machine, compute_wake_str=compute_wake_str,
                                       f_params_dict=self.f_params_dict, z_params_dict=self.z_params_dict,
                                       additional_f_params=self.additional_f_params)

    def create_elements_list(self) -> List[Collimator]:
        elements_list = []
        tfs = pyoptics.optics.open(self.optics_filename)

        for name in self.names:
            n_sigma = None
            if 'n_sigma' in self.settings_dict[name].keys():
                n_sigma = self.settings_dict[name]['n_sigma']

            halfgap = None
            if 'halfgap' in self.settings_dict[name].keys():
                halfgap = self.settings_dict[name]['halfgap']

            if (halfgap is not None) + (n_sigma is not None) != 1:
                raise ValueError(
                    f'Error in collimator {name} parameters: one and only one between halfgap and n_sigma '
                    f'must be specified')

            if 'name_for_twiss_file' in self.settings_dict[name]:
                name_for_tfs = self.settings_dict[name]['name_for_twiss_file']
            else:
                name_for_tfs = name

            if halfgap is None:
                halfgap_one_sigma = self.compute_one_sigma_halfgap(
                    beta_x=tfs.betx[tfs.name == name_for_tfs.lower()][0],
                    beta_y=tfs.bety[tfs.name == name_for_tfs.lower()][0],
                    angle=self.settings_dict[name]['angle'])
                halfgap = n_sigma * halfgap_one_sigma

            additional_resonator_parameters = {'z0000': [], 'x1000': [], 'y0100': [], 'x0010': [], 'y0001': []}

            if 'halfgap_dependent_hom_files' in self.settings_dict[name].keys():
                for halfgap_dependent_hom_file in self.settings_dict[name]['halfgap_dependent_hom_files']:
                    hom_dict = halfgap_dependent_hom(halfgap=halfgap, hom_filename=halfgap_dependent_hom_file)
                    for comp in hom_dict:
                        if hom_dict[comp]['r'] > 0:
                            additional_resonator_parameters[comp].append({'r': hom_dict[comp]['r'],
                                                                          'q': hom_dict[comp]['q'],
                                                                          'f': hom_dict[comp]['f']})

            if 'asymmetry_factor' in self.settings_dict[name].keys():
                asymmetry_factor = self.settings_dict[name]['asymmetry_factor']
            else:
                asymmetry_factor = None

            elements_list.append(Collimator(
                angle=self.settings_dict[name]['angle'],
                layers=self.settings_dict[name]['layers'],
                length=self.settings_dict[name]['length'],
                beta_x=tfs.betx[tfs.name == name_for_tfs.lower()][0],
                beta_y=tfs.bety[tfs.name == name_for_tfs.lower()][0],
                halfgap=halfgap, name=name, additional_resonator_parameters=additional_resonator_parameters,
                relativistic_gamma=self.relativistic_gamma,
                tapers=self.taper_data[self.settings_dict[name]['taper_type']],
                use_single_layer_approx_for_coated_taper=self.use_single_layer_approx_for_coated_taper,
                asymmetry_factor=asymmetry_factor, resonator_f_roi_level=self.resonator_f_roi_level,
                frequency_parameters_for_taper_rw=self.frequency_parameters_for_taper_rw))

        return elements_list

    def launch_rw_simulations(self):
        # Here we create the condor input files and launch the simulations
        projects_path = Path(get_iw2d_config_value('project_directory'))
        bin_path = Path(get_iw2d_config_value('binary_directory'))
        hashes_dict = {}

        # Create a folder for each collimator
        for coll in self.elements_list:
            iw2d_input = coll.iw2d_input

            already_computed, input_hash, coll_dir = check_already_computed(iw2d_input, coll.name)
            hashes_dict[coll.name] = {'input_hash': input_hash}

            if already_computed:
                hashes_dict[coll.name]['already_computed'] = True
                continue
            else:
                hashes_dict[coll.name]['already_computed'] = False

            add_iw2d_input_to_database(iw2d_input, input_hash, coll_dir)

            executable_file_name = os.path.join(coll_dir, 'iw2d_job.txt')

            executable = 'flatchamber.x'
            if self.compute_wake:
                executable = "wake_" + executable

            text = f"#!/bin/bash\n" \
                   f"{bin_path}/{executable} < {coll_dir}/input.txt\n"

            with open(executable_file_name, 'w') as executable_file:
                executable_file.write(text)

        # Create the job sub file
        jobs_submission_function_args = inspect.getfullargspec(self.jobs_submission_function).args
        if set(jobs_submission_function_args) != {'hashes_dict', 'sub_file_name'}:
            raise TypeError('Job submission function must take the arguments hashes_dict,'
                            'sub_file_name')
        sub_file_name = os.path.join(projects_path, 'collimators.sub')
        self.jobs_submission_function(hashes_dict=hashes_dict,
                                      sub_file_name=sub_file_name)

    def retrieve_rw_simulations(self):
        for collimator in self.elements_list:
            collimator.retrieve_rw_simulations()

    def add_geometric_impedance(self):
        for collimator in self.elements_list:
            collimator.add_geometric_impedance()

    def add_taper_RW_impedance(self):
        for collimator in self.elements_list:
            collimator.add_taper_RW_impedance()

    def launch_or_retrieve(self):
        if self.lxplusbatch in ['launch', None]:
            self.launch_rw_simulations()
        if self.lxplusbatch in ['retrieve', None]:
            self.retrieve_rw_simulations()
            if self.compute_geometric_impedance:
                self.add_geometric_impedance()
            if self.compute_taper_RW_impedance:
                self.add_taper_RW_impedance()

    def compute_one_sigma_halfgap(self, beta_x, beta_y, angle):
        if self.normalized_emittance is None:
            raise ValueError('The normalized emittance must be specified in order to compute the one sigma half-gap')

        if self.relativistic_gamma is None:
            raise ValueError('The relativistic gamma factor must be specified in order to compute the one sigma '
                             'half-gap')

        relativistic_beta = np.sqrt(1 - 1 / self.relativistic_gamma**2)

        emit = self.normalized_emittance * 1e-6

        return np.sqrt(beta_x * emit * np.cos(angle)**2 / (relativistic_beta * self.relativistic_gamma) +
                       beta_y * emit * np.sin(angle)**2 / (relativistic_beta * self.relativistic_gamma))
