from __future__ import annotations

import os

from lhc_pywit_model.utils import execute_jobs_on_htcondor
from lhc_pywit_model.collimators_group import CollimatorsGroup

from pywit.element import Element

from typing import List, Union, Callable
from pathlib import Path


class RomanPotsGroup(CollimatorsGroup):
    """
    An ElementGroup made of many roman pots. The collimators parameters are specified in a text file containing a table
    with the collimators attributes (Name, Betax, Betay, sigx, sigy, tilt1, tilt2, Angle, Material, thickness, Halfgap,
    Length, ID, nsig)
    """

    def __init__(self, settings_filename: str, lxplusbatch: str, compute_wake: bool, relativistic_gamma: float,
                 jobs_input_files_directory: Union[str, Path], path_to_iw2d_executables: Union[str, Path],
                 f_params_dict: dict, z_params_dict: dict,
                 jobs_submission_function: Callable = execute_jobs_on_htcondor,
                 submit_jobs: bool = True, machine: str = 'LHC', scenario: str = '', name: str = '',
                 force_recompute_scenario: bool = False):

        super().__init__(settings_filename=settings_filename, lxplusbatch=lxplusbatch, compute_wake=compute_wake,
                         relativistic_gamma=relativistic_gamma, jobs_input_files_directory=jobs_input_files_directory,
                         path_to_iw2d_executables=path_to_iw2d_executables, f_params_dict=f_params_dict,
                         z_params_dict=z_params_dict, jobs_submission_function=jobs_submission_function,
                         submit_jobs=submit_jobs, machine=machine, scenario=scenario, name=name,
                         force_recompute_scenario=force_recompute_scenario, flag_add_geometric_impedance=False,
                         tctp_mode_filename='')

    def create_elements_list(self) -> List[Element]:
        elements_list = []
        for ind_name, name in enumerate(self.names):
            elements_list.append(Element(length=self.settings_dict[name]['length'],
                                         beta_x=self.settings_dict[name]['beta_x'],
                                         beta_y=self.settings_dict[name]['beta_y'],
                                         name=name + '_' + 'RW',
                                         ))

        return elements_list

    def create_directories(self):
        # Directory containing the jobs inputs for all the collimators
        self.jobs_input_files_collimators_directory = os.path.join(self.jobs_input_files_directory, 'roman_pots')

        # Directory containing the job inputs for all the collimators in this scenario
        self.jobs_input_files_collimators_scenario_directory = os.path.join(
            self.jobs_input_files_collimators_directory,
            self.scenario
        )
        self.scenario_exists = os.path.exists(self.jobs_input_files_collimators_scenario_directory)

        if not os.path.exists(self.jobs_input_files_directory):
            os.mkdir(self.jobs_input_files_directory)
        if not os.path.exists(self.jobs_input_files_collimators_directory):
            os.mkdir(self.jobs_input_files_collimators_directory)
        if not self.scenario_exists:
            os.mkdir(self.jobs_input_files_collimators_scenario_directory)

    def launch_or_retrieve(self):
        if self.lxplusbatch in ['launch', None] and (self.force_recompute_scenario or not self.scenario_exists):
            self.launch_rw_simulations()
        if self.lxplusbatch in ['retrieve', None] or (not self.force_recompute_scenario and self.scenario_exists):
            self.retrieve_rw_simulations()
