from __future__ import annotations

from pywit.elements_group import ElementsGroup
from pywit.utilities import create_resonator_element

from typing import Dict


class BroadbandResonatorsGroup(ElementsGroup):
    """
    A group of broadband resonators. The impedance of the elements in this group is constant up to a user-specified
    cut-off frequency.
    """
    def __init__(self, parameters_dict: Dict[str, Dict[str, float]], betas_lengths_dict: Dict[str, Dict[str, float]],
                 name: str, f_cutoff_broadband: float = 50e9):
        """
        :param parameters_dict: A dictionary associating to each device a dictionary indicating the parameters Rl
        (longitudinal broadband shunt impedance in Ohm), Rt (transverse broadband impedance in Ohm/m, assumed to be the
        same in x and y), length (the length of each device in meters - if multiple devices here the length of a single
        device has to be indicated)
        E.g.: parameters_dict = {
        ...
            'device_name': {length: ..., 'Rl': ..., 'Rt': ...}
        ...
        }
        The initialization function for the BroadbandResonatorsGroup class.
        :param betas_lengths_dict: A dictionary associating to each device a dictionary containing
        the beta_x and beta_y functions and the length of the device (if there are multiple devices with the same
        broadband impedance, e.g. many RF cavities, it will have to indicate the cumulative length of all such devices).
        E.g.: betas_lengths_dict = {
        ...
            'device_name': {'length': ..., 'beta_x': ..., 'beta_y': ...}
        ...
        }
        :param name: A string indicating the name of the group
        :param f_cutoff_broadband: The cutoff frequency used for the broad-band impedances
        """
        self.name = name
        self.f_cutoff_broadband = f_cutoff_broadband

        # read resonator parameters
        self.parameters_dict = parameters_dict

        self.elements_list = []

        for device_name in self.parameters_dict.keys():
            total_device_length = betas_lengths_dict[device_name]['length']
            beta_x_device = betas_lengths_dict[device_name]['beta_x']
            beta_y_device = betas_lengths_dict[device_name]['beta_y']

            single_device_length = self.parameters_dict[device_name]['length']
            number_of_devices = total_device_length / single_device_length

            rs = {
                'z0000': float(self.parameters_dict[device_name]['Rl']),
                'x1000': float(self.parameters_dict[device_name]['Rt']),
                'y0100': float(self.parameters_dict[device_name]['Rt']),
            }

            fs = {
                'z0000': self.f_cutoff_broadband,
                'x1000': self.f_cutoff_broadband,
                'y0100': self.f_cutoff_broadband,
            }

            qs = {
                'z0000': 1,
                'x1000': 1,
                'y0100': 1,
            }

            # we create a single element and then we multiply it by the number of devices. We need to divide the length
            # by the number of devices because the length is multiplied again in pywit.
            self.elements_list.append(
                create_resonator_element(length=single_device_length, beta_x=beta_x_device,
                                         beta_y=beta_y_device, rs=rs, qs=qs, fs=fs) * number_of_devices
            )
            self.elements_list[-1].name = f'{device_name} broadband impedance'

        super().__init__(elements_list=self.elements_list, name=self.name)
