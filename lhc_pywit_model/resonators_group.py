from __future__ import annotations

from pywit.element import Element
from pywit.utilities import create_many_resonators_element

from lhc_pywit_model.parameters import DEFAULT_RESONATOR_F_ROI_LEVEL

from typing import Dict


class ResonatorsGroup(Element):
    """
    A group of resonators, which is meant to store many resonant modes for a device. Each of them is modeled as a
    separate element.
    """
    def __init__(self, parameters_dict: Dict[str, Dict[str, float]],
                 betas_lengths_dict: Dict[str, Dict[str, float]], name: str = '',
                 resonator_f_roi_level: float = DEFAULT_RESONATOR_F_ROI_LEVEL):
        """
        The initialization function for the ResonatorsGroup class.
        :param betas_lengths_dict: A dictionary associating to each device a dictionary containing
        the beta_x and beta_y functions and the length of the device (if there are multiple devices with the same
        broadband impedance, e.g. many RF cavities, it will have to indicate the cumulative length of all such devices).
        E.g.: betas_lengths_dict = {
        ...
            'device_name': {'length': ..., 'beta_x': ..., 'beta_y': ...}
        ...
        }
        :param parameters_dict: The path to a json file with the following items:
            - name: a string indicating the name of the device
            - length: (the length of the device in meters - if multiple devices here the
                       length of a single device has to be indicated)
            - modes: a list of dictionaries indicating the parameters of each mode associated
            with the device:
                * Rl, the longitudinal shunt impedance in Ohm
                * Ql, the longitudinal Q-factor (adimensional)
                * fl, the resonant frequency of the longitudinal component
                * Rxd, the shunt impedance of the x component in Ohm
                * Qxd, the Q-factor of the x component(adimensional)
                * fxd, the resonant frequency of the x component in Hz
                * Ryd, the shunt impedance of the y component in Ohm
                * Qyd, the Q-factor of the y component (adimensional)
                * fyd, the resonant frequency of the x component in Hz
        E.g. parameters_dict = {
            'name': ...,
            'length': ...,
            'modes': [
            ...
            {
                'Rl': ...,
                'Ql': ...,
                'fl': ...,
                'Rxd': ...,
                'Qxd': ...,
                'fxd': ...,
                'Ryd': ...,
                'Qyd': ...,
                'fyd': ...
              }
            }
        """
        self.elements_list = []
        self.parameters_dict = parameters_dict
        self.single_device_length = self.parameters_dict['length']
        self.name = name

        self.length = betas_lengths_dict[self.parameters_dict['name']]['length']
        self.beta_x = betas_lengths_dict[self.parameters_dict['name']]['beta_x']
        self.beta_y = betas_lengths_dict[self.parameters_dict['name']]['beta_y']

        self.number_of_devices = self.length / self.single_device_length

        params_dict = {'z0000': [], 'x1000': [], 'y0100': []}

        for i_mode, mode_parameters in enumerate(self.parameters_dict['modes']):
            params_dict['z0000'].append(
                {
                    'r': float(mode_parameters['Rl']),
                    'q': float(mode_parameters['Ql']),
                    'f': float(mode_parameters['fl']),
                    'f_roi_level': resonator_f_roi_level
                }
            )

            params_dict['x1000'].append(
                {
                    'r': float(mode_parameters['Rxd']),
                    'q': float(mode_parameters['Qxd']),
                    'f': float(mode_parameters['fxd']),
                    'f_roi_level': resonator_f_roi_level
                }
            )

            params_dict['y0100'].append(
                {
                    'r': float(mode_parameters['Ryd']),
                    'q': float(mode_parameters['Qyd']),
                    'f': float(mode_parameters['fyd']),
                    'f_roi_level': resonator_f_roi_level
                }
            )

        res_elem = create_many_resonators_element(params_dict=params_dict,
                                                  length=self.single_device_length / self.number_of_devices,
                                                  beta_x=self.beta_x, beta_y=self.beta_y) * self.number_of_devices

        super().__init__(components=res_elem.components,
                         beta_x=res_elem.beta_x, beta_y=res_elem.beta_y,
                         length=res_elem.length, name=self.name)
