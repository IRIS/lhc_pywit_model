from __future__ import annotations

from lhc_pywit_model.elements_group import ElementsGroup
from lhc_pywit_model.elliptic_elements_group import MostacciPumpingHolesElement
from typing import Dict


from scipy.constants import c, mu_0

z_0 = mu_0 * c


class PumpingHolesGroup(ElementsGroup):
    def __init__(self, beta_length_dict: Dict[str, Dict[str, float]], parameters_dict: Dict[str, Dict[str, float]],
                 name: str, model='mostacci', f_cutoff: float = 50e9):
        self.name = name
        self.model = model
        self.elements_list = []
        self.f_cutoff = f_cutoff
        self.parameters_dict = parameters_dict

        self.beta_length_dict = beta_length_dict

        if model == 'mostacci':
            for bs_section in parameters_dict.keys():
                section_dict = parameters_dict[bs_section]
                self.elements_list.append(MostacciPumpingHolesElement(
                    lh=section_dict['Lh'], wh=section_dict['Wh'], t=section_dict['T'], b=section_dict['b'],
                    d=section_dict['d'], eta=section_dict['eta'], rhob=section_dict['rhob'], rhod=section_dict['rhod'],
                    number_of_holes_per_crosssection=section_dict['nb_holes_per_cs'],
                    length=self.beta_length_dict[bs_section]['length'], cm=1, ce=1, f_cutoff=self.f_cutoff,
                    beta_x=self.beta_length_dict[bs_section]['beta_x'],
                    beta_y=self.beta_length_dict[bs_section]['beta_y']
                ).element)
        else:
            raise ValueError('Only Mostacci model is implemented')

        super().__init__(elements_list=self.elements_list, name=self.name)
