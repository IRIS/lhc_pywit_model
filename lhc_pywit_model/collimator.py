from __future__ import annotations

import os

import numpy as np
import json

from pywit.component import Component
from pywit.element import Element
from pywit.interface import (import_data_iw2d, create_component_from_data, _create_iw2d_input_from_dict,
                             check_already_computed)
from pywit.interface import FlatIW2DInput, Sampling, component_names
from IW2D.interface import RoundIW2DInput, IW2DLayer, Mu1FromSusceptibility, Eps1FromResistivity
from IW2D.round_impedance import _iw2d_round_impedance_single_frequency
from pywit.utilities import (create_resonator_component, string_to_params,
                             create_taper_RW_approx_component,
                             create_resistive_wall_single_layer_approx_component,
                             create_interpolated_impedance_component)
from pywit.materials import layer_from_json_material_library
from pywit.devices import shunt_impedance_flat_taper_stupakov_formula

from lhc_pywit_model.utils import create_layer_from_dict
from lhc_pywit_model.parameters import DEFAULT_RESONATOR_F_ROI_LEVEL
from lhc_pywit_model.package_paths import data_directory

from typing import Dict, Sequence, List, Tuple


class Collimator(Element):
    def __init__(self, angle: float, layers: Sequence[Dict], halfgap: float, length: float, beta_x: float,
                 beta_y: float, relativistic_gamma: float,
                 tapers: Sequence[Dict] = None, use_single_layer_approx_for_coated_taper: bool = True,
                 asymmetry_factor: float = None,
                 resonator_f_roi_level: float = DEFAULT_RESONATOR_F_ROI_LEVEL,
                 additional_resonator_parameters: Dict[str, List[Dict[str, float]]] = None,
                 f_cutoff_bb: float = 50e9, name: str = "Unnamed Collimator", tag: str = "", description: str = "",
                 frequency_parameters_for_taper_rw: dict = None):
        r"""
        The initialization function of the Collimator class
        :param angle: the angle indicating the orientation of a collimator (e.g. it is 0 for a horizontal collimator and
        $\frac{\pi}{2}$ for a vertical collimator). Note: a horizontal collimator collimates the beam horizontally,
        meaning its jaws are vertical (and the highest impedance is in the horizontal plane).
        :param layers: a list of dictionaries indicating the following parameters for each layer of the collimator:
                * material: the material of the layer (the material must be present either in the json or in the python
                in the materials library)
                * thickness: the thickness of the coating
        :param halfgap: the half-gap of the collimator in meters
        :param length: the length of the collimator
        :param beta_x: the size of the beta function in the x-plane at the position of the Element, must be specified
        for valid initialization
        :param beta_y: the size of the beta function in the y-plane at the position of the Element, must be specified
        for valid initialization
        :param relativistic_gamma: the relativistic $\gamma$ factor of the beam
        :param tapers: list of dictionaries containing the length, the width and the delta (i.e. the
        difference in aperture between the larger and narrower ends of the taper) for each taper of the collimator,
        as well as the materials in which the taper is made (for the RW part), as follows:
          [
            {"length": ..., "delta_halfgap": ..., "width": ...,
             "layers": [{"material": ..., "thickness": ...}, {"material": ..., "thickness": ...}],
             },
            {"length": ..., "delta_halfgap": ..., "width": ...,
             "layers": [{"material": ..., "thickness": ...}],
             }
          ]
        Flat parts (with zero slope, i.e. showing no geometric impedance but only
        RW impedance), can be included by putting delta_halfgap=0.
        :param use_single_layer_approx_for_coated_taper: a bool specifying if we use only a single layer for
        coated tapers (RW part) - otherwise calculations are much slower (and overall impact is small)
        :param additional_resonator_parameters: the parameters of additional resonator components, which have to be
        passed as a dictionary associating to each component a list of dictionaries containing the
        parameters of a resonator component (shunt impedance, frequency and Q factor). E.g.:
        additional_resonator_parameters = {
            'z0000':
                [
                    {'r': 10, 'q': 10, 'f': 50},
                    {'r': 40, 'q': 100, 'f': 60}
                ],
            'x1000':
            ...
        }
        :param f_cutoff_bb: The cutoff frequency used for the geometric taper impedance
        :param name: A user-specified name of the Collimator
        :param tag: An optional keyword that can be used to help grouping different Collimator objects
        :param description: A user-specified description of the Collimator
        :param frequency_parameters_for_taper_rw: a dictionary of parameters used in the interpolation of the taper
        resistive wall component impedance function. The dictionary must contain the following items:
            * freq_start: the lowest frequency of the interpolation
            * freq_stop: the highest frequency of the interpolation
            * num_points: the number of interpolation points
        The interpolation frequencies are then calculated as np.geomspace(freq_start, freq_stop, num_points)
        """
        self.name = name
        self.halfgap = halfgap
        self.angle = angle
        self.layers = layers
        self.relativistic_gamma = relativistic_gamma
        self.f_cutoff_bb = f_cutoff_bb

        self.use_single_layer_approx_for_coated_taper = use_single_layer_approx_for_coated_taper
        self.additional_resonator_parameters = additional_resonator_parameters
        self.asymmetry_factor = asymmetry_factor
        self.frequency_parameters_for_taper_rw = frequency_parameters_for_taper_rw

        super().__init__(length=length, beta_x=beta_x, beta_y=beta_y, components=[], name=name, tag=tag,
                         description=description)

        self.iw2d_input = None

        self.resonator_f_roi_level = resonator_f_roi_level

        self.tapers = tapers

        if self.tapers is not None:
            r_shunt_bb_dict = taper_geometric_impedance(halfgap=halfgap, tapers=self.tapers,
                                                        f_cutoff_bb=self.f_cutoff_bb)
            additional_resonator_parameters['z0000'].append({'r': r_shunt_bb_dict['zlong'],
                                                             'f': self.f_cutoff_bb,
                                                             'q': 1})
            additional_resonator_parameters['x1000'].append({'r': r_shunt_bb_dict['zxdip'],
                                                             'f': self.f_cutoff_bb,
                                                             'q': 1})
            additional_resonator_parameters['y0100'].append({'r': r_shunt_bb_dict['zydip'],
                                                             'f': self.f_cutoff_bb,
                                                             'q': 1})
            additional_resonator_parameters['x0010'].append({'r': r_shunt_bb_dict['zxqua'],
                                                             'f': self.f_cutoff_bb,
                                                             'q': 1})
            additional_resonator_parameters['y0001'].append({'r': r_shunt_bb_dict['zyqua'],
                                                             'f': self.f_cutoff_bb,
                                                             'q': 1})

    def add_contribution(self, other: Element):
        elem_res = self + other
        self.components = elem_res.components

    def add_geometric_impedance(self):
        components_list = []

        for comp_id in self.additional_resonator_parameters.keys():
            plane, exponents = string_to_params(comp_id, include_is_impedance=False)
            component = 0
            for dict_settings in self.additional_resonator_parameters[comp_id]:
                component += create_resonator_component(plane=plane, exponents=exponents, r=dict_settings['r'],
                                                        q=dict_settings['q'], f_r=dict_settings['f'],
                                                        f_roi_level=self.resonator_f_roi_level)

            if len(self.additional_resonator_parameters[comp_id]) > 0:
                components_list.append(component)

        if len(components_list) == 0:
            return

        self.add_contribution(Element(length=self.length,
                                      beta_x=self.beta_x,
                                      beta_y=self.beta_y,
                                      name=self.name,
                                      components=components_list
                                      ).rotated(np.pi / 2 - self.angle, rotate_beta=False))

    def add_taper_RW_impedance(self):
        taper_rw_components = full_taper_RW_impedance(
            self.halfgap,
            self.relativistic_gamma, self.tapers, self.asymmetry_factor,
            use_single_layer_approx_for_coated_taper=self.use_single_layer_approx_for_coated_taper
        )

        if self.frequency_parameters_for_taper_rw is not None:

            interpolated_components = []

            interpolation_frequencies = np.geomspace(self.frequency_parameters_for_taper_rw['freq_start'],
                                                     self.frequency_parameters_for_taper_rw['freq_stop'],
                                                     self.frequency_parameters_for_taper_rw['num_points'])

            for taper_rw_component in taper_rw_components:
                interpolated_components.append(create_interpolated_impedance_component(
                    interpolation_frequencies=interpolation_frequencies,
                    impedance=taper_rw_component.impedance,
                    wake=taper_rw_component.wake, plane=taper_rw_component.plane,
                    source_exponents=taper_rw_component.source_exponents,
                    test_exponents=taper_rw_component.test_exponents,
                    name=taper_rw_component.name,
                    f_rois=taper_rw_component.f_rois,
                    t_rois=taper_rw_component.t_rois
                ))

            self.add_contribution(Element(length=self.length,
                                          beta_x=self.beta_x,
                                          beta_y=self.beta_y,
                                          name=self.name,
                                          components=interpolated_components
                                          ).rotated(np.pi / 2 - self.angle, rotate_beta=False))
        else:

            self.add_contribution(Element(length=self.length,
                                          beta_x=self.beta_x,
                                          beta_y=self.beta_y,
                                          name=self.name,
                                          components=taper_rw_components
                                          ).rotated(np.pi / 2 - self.angle, rotate_beta=False))

    def init_iw2d_input(self, machine: str, compute_wake_str: str, f_params_dict: dict,
                        z_params_dict: dict, additional_f_params: dict):

        halfgap = self.halfgap

        # for every collimator find the layers and construct a dictionary of inputs for IW2D
        dict_coll = {'comment': '',
                     'machine': machine,
                     'is_round': 'false',
                     'top_layers': [],
                     'top_bottom_symmetry': 'true',
                     'calculate_wake': compute_wake_str,
                     'length': self.length,
                     'relativistic_gamma': self.relativistic_gamma,
                     'top_half_gap': halfgap,
                     'f_params': f_params_dict.copy(),
                     'z_params': z_params_dict.copy()
                     }

        if additional_f_params is not None:
            dict_coll.update(additional_f_params)

        for layer_dict in self.layers:
            layer = create_layer_from_dict(layer_dict)
            dict_coll['top_layers'].append(layer.__dict__)

        dict_coll['top_layers'].append(layer_from_json_material_library(
            thickness=np.inf,
            material_key='stainless_steel_304l').__dict__)

        if self.asymmetry_factor is not None:
            dict_coll['top_bottom_symmetry'] = 'false'
            if self.asymmetry_factor != 0:
                dict_coll['bottom_layers'] = dict_coll['top_layers'].copy()
                dict_coll['bottom_half_gap'] = (self.asymmetry_factor * dict_coll['top_half_gap'])
            else:
                dict_coll['bottom_layers'] = []
                # in this case PyWIT still wants a value for the half-gap even when there are no layers
                dict_coll['bottom_half_gap'] = np.inf

        self.iw2d_input = _create_iw2d_input_from_dict(dict_coll)

    def retrieve_rw_simulations(self):
        already_computed, _, coll_dir = check_already_computed(self.iw2d_input, self.name)
        if not already_computed:
            raise ValueError(f'Collimator {self.name} not computed')

        data_iw2d_list = import_data_iw2d(directory=coll_dir, common_string='')
        components_list = []
        for data_iw2d in data_iw2d_list:
            components_list.append(create_component_from_data(*data_iw2d,
                                                              relativistic_gamma=self.relativistic_gamma))

        self.add_contribution(Element(length=self.length,
                                      beta_x=self.beta_x,
                                      beta_y=self.beta_y,
                                      name=self.name,
                                      components=components_list
                                      ).rotated(np.pi / 2 - self.angle)
                              )


def taper_geometric_impedance(halfgap, tapers, f_cutoff_bb):
    component_ids = ['zlong', 'zxdip', 'zydip', 'zxqua', 'zyqua']
    r_shunt_bb_dict = {comp_id: 0. for comp_id in component_ids}

    sum_delta = halfgap
    halfgap_tot = halfgap
    for taper in tapers:
        delta = taper['delta_halfgap']
        w = taper['width']
        if delta > 0.:
            sum_delta += delta
            length = taper['length']
            for comp_id in component_ids:
                r_shunt_bb_dict[comp_id] += 2 * shunt_impedance_flat_taper_stupakov_formula(
                    half_gap_small=halfgap_tot,
                    half_gap_big=sum_delta,
                    taper_slope=delta / length,
                    half_width=w,
                    cutoff_frequency=f_cutoff_bb,
                    component_id=comp_id,
                    approximate_integrals=True
                )
            halfgap_tot += delta

    return r_shunt_bb_dict


def create_IW2DLayer_from_dict(layer_dict):
    """
    Create an IW2D layer.
    TODO: improve IW2D/PyWIT interface and use it instead.
    """
    layer = create_layer_from_dict(layer_dict)  # this is a PyWIT layer
    # -> convert it to a IW2DLayer
    return IW2DLayer(layer.thickness,
                     Eps1FromResistivity(layer.dc_resistivity,
                                         layer.resistivity_relaxation_time,
                                         layer.re_dielectric_constant),
                     Mu1FromSusceptibility(layer.magnetic_susceptibility,
                                           layer.permeability_relaxation_frequency)
                     )


def create_resistive_wall_component(plane: str,
                                    exponents: Tuple[int, int, int, int],
                                    input_data: RoundIW2DInput) -> Component:
    """
    Create a multilayer flat RW impedance, without approximation,
    apart from the Yokoya factors. Simple wrapper around an IW2D function.
    TODO: improve IW2D/PyWIT interface and use it instead.
    """
    # Longitudinal impedance
    if plane == 'z' and exponents == (0, 0, 0, 0):
        impedance = lambda frequencies: np.array([_iw2d_round_impedance_single_frequency(input_data, float(f))[0]
                                                  for f in frequencies])
    # Transverse impedances
    elif plane == 'x' and exponents == (1, 0, 0, 0):
        impedance = lambda frequencies: np.array([_iw2d_round_impedance_single_frequency(input_data, float(f))[1]
                                                  for f in frequencies])
    elif plane == 'y' and exponents == (0, 1, 0, 0):
        impedance = lambda frequencies: np.array([_iw2d_round_impedance_single_frequency(input_data, float(f))[2]
                                                  for f in frequencies])
    elif plane == 'x' and exponents == (0, 0, 1, 0):
        impedance = lambda frequencies: np.array([_iw2d_round_impedance_single_frequency(input_data, float(f))[3]
                                                  for f in frequencies])
    elif plane == 'y' and exponents == (0, 0, 0, 1):
        impedance = lambda frequencies: np.array([_iw2d_round_impedance_single_frequency(input_data, float(f))[4]
                                                  for f in frequencies])
    else:
        impedance = lambda frequencies: 0

    return Component(impedance=impedance, plane=plane,
                     source_exponents=exponents[:2], test_exponents=exponents[2:])


def create_taper_RW_component(plane: str, exponents: Tuple[int, int, int, int],
                              length, relativistic_gamma, layers,
                              half_gap_small, half_gap_big, step_size=1e-3,
                              asymmetry_factor=1.) -> Component:
    """
    Create a multilayer flat taper RW impedance, integrating over half-gaps
    IW2D round calculations (with Yokoya factors).
    TODO: improve IW2D/PyWIT interface and use it instead.
    """
    npts = int(np.floor(abs(half_gap_big - half_gap_small) / step_size) + 1)
    halfgaps = np.linspace(half_gap_small, half_gap_big, npts)

    def impedance(frequencies):
        Z = []
        for halfgap in halfgaps:
            input_data = RoundIW2DInput(
                length=length / npts,
                relativistic_gamma=relativistic_gamma,
                calculate_wake=False,
                layers=layers,
                inner_layer_radius=halfgap,
                yokoya_Zlong=1.,
                yokoya_Zxdip=0.25 if asymmetry_factor == 0. else np.pi ** 2 / 24.,
                yokoya_Zydip=0.25 if asymmetry_factor == 0. else np.pi ** 2 / 12.,
                yokoya_Zxquad=-0.25 if asymmetry_factor == 0. else -np.pi ** 2 / 24.,
                yokoya_Zyquad=0.25 if asymmetry_factor == 0. else np.pi ** 2 / 24.,
            )
            # Longitudinal impedance
            if plane == 'z' and exponents == (0, 0, 0, 0):
                Z.append(np.array([_iw2d_round_impedance_single_frequency(input_data, float(f))[0]
                                   for f in frequencies]))
            # Transverse impedances
            elif plane == 'x' and exponents == (1, 0, 0, 0):
                Z.append(np.array([_iw2d_round_impedance_single_frequency(input_data, float(f))[1]
                                   for f in frequencies]))
            elif plane == 'y' and exponents == (0, 1, 0, 0):
                Z.append(np.array([_iw2d_round_impedance_single_frequency(input_data, float(f))[2]
                                   for f in frequencies]))
            elif plane == 'x' and exponents == (0, 0, 1, 0):
                Z.append(np.array([_iw2d_round_impedance_single_frequency(input_data, float(f))[3]
                                   for f in frequencies]))
            elif plane == 'y' and exponents == (0, 0, 0, 1):
                Z.append(np.array([_iw2d_round_impedance_single_frequency(input_data, float(f))[4]
                                   for f in frequencies]))

        return np.trapz(np.array(Z), axis=0)

    return Component(impedance=impedance, plane=plane,
                     source_exponents=exponents[:2], test_exponents=exponents[2:])


def single_taper_RW_impedance(component_id, half_gap_small, half_gap_big, length,
                              relativistic_gamma, layers,
                              step_size=5e-4, asymmetry_factor=1.,
                              ):
    if asymmetry_factor not in [0., 1., None]:
        print(f"Assymmetry factor {asymmetry_factor} cannot be taken into"
              " account fully for taper RW impedance (only single jaw "
              " or sym. jaws can be) -> will assume jaws are symmetric")
    _, plane, exponents = component_names[component_id]
    component = 0.
    if len(layers) == 1:
        # single layer -> we use the approximated RW formula
        input_data = FlatIW2DInput(
            machine='LHC',
            length=length,
            relativistic_gamma=relativistic_gamma,
            calculate_wake=False,
            f_params=Sampling(1e3, 1e13, 0, added=(), points_per_decade=0),
            # f_params is actually not used
            top_bottom_symmetry=False if asymmetry_factor == 0. else True,
            top_layers=layers,
            top_half_gap=half_gap_small,  # not used
            bottom_layers=[] if asymmetry_factor == 0. else None,
            bottom_half_gap=np.inf if asymmetry_factor == 0. else None,
        )
        component += create_taper_RW_approx_component(plane=plane, exponents=exponents, input_data=input_data,
                                                      radius_small=half_gap_small, radius_large=half_gap_big,
                                                      step_size=step_size)
    else:
        # more than one layer -> we use round chamber IW2D with form factors
        # (Yokoya factors for sym. jaws, Burov-Danilov for a single jaw)
        component += create_taper_RW_component(plane=plane, exponents=exponents,
                                               length=length, relativistic_gamma=relativistic_gamma,
                                               layers=layers, half_gap_small=half_gap_small, half_gap_big=half_gap_big,
                                               step_size=step_size, asymmetry_factor=asymmetry_factor)

    return component


def flat_part_RW_impedance(component_id, half_gap, length,
                           relativistic_gamma, layers,
                           asymmetry_factor=1.,
                           ):
    # note: taper can have zero slope when half_gap_small==half_gap_big
    # -> used to simulate a flat part in the taper (e.g. with BPM button);
    # in this case npts=1
    if asymmetry_factor not in [0., 1., None]:
        print(f"Assymmetry factor {asymmetry_factor} cannot be taken into"
              " account fully for taper RW impedance (only single jaw "
              " or sym. jaws can be) -> will assume jaws are symmetric")
    _, plane, exponents = component_names[component_id]
    component = 0.
    if len(layers) == 1:
        # single layer -> we use the approximated RW formula
        input_data = FlatIW2DInput(
            machine='LHC',
            length=length,
            relativistic_gamma=relativistic_gamma,
            calculate_wake=False,
            f_params=Sampling(1e3, 1e13, 0, added=(), points_per_decade=0),
            # f_params is actually not used
            top_bottom_symmetry=False if asymmetry_factor == 0. else True,
            top_layers=layers,
            top_half_gap=half_gap,
            bottom_layers=[] if asymmetry_factor == 0. else None,
            bottom_half_gap=np.inf if asymmetry_factor == 0. else None,
        )
        component += create_resistive_wall_single_layer_approx_component(
            plane, exponents, input_data)
    else:
        # more than one layer -> we use round chamber IW2D with form factors
        # (Yokoya factors for sym. jaws, Burov-Danilov for a single jaw)
        input_data = RoundIW2DInput(
            length=length,
            relativistic_gamma=relativistic_gamma,
            calculate_wake=False,
            layers=layers,
            inner_layer_radius=half_gap,
            yokoya_Zlong=1.,
            yokoya_Zxdip=0.25 if asymmetry_factor == 0. else np.pi ** 2 / 24.,
            yokoya_Zydip=0.25 if asymmetry_factor == 0. else np.pi ** 2 / 12.,
            yokoya_Zxquad=-0.25 if asymmetry_factor == 0. else -np.pi ** 2 / 24.,
            yokoya_Zyquad=0.25 if asymmetry_factor == 0. else np.pi ** 2 / 24.,
        )
        component += create_resistive_wall_component(
            plane, exponents, input_data)

    return component


def full_taper_RW_impedance(halfgap, relativistic_gamma, tapers, asymmetry_factor,
                            use_single_layer_approx_for_coated_taper=True) -> List[Component]:
    component_ids = ['zlong', 'zxdip', 'zydip', 'zxqua', 'zyqua']
    taper_RW_components = []

    sum_delta = halfgap
    halfgap_tot = halfgap
    for taper in tapers:
        if len(taper['layers']) > 1 and (not use_single_layer_approx_for_coated_taper):
            # multilayer case - use IW2DLayer, not the PyWIT one
            # TODO: harmonize the interface
            layers = [create_IW2DLayer_from_dict(layer_dict)
                      for layer_dict in taper['layers']]
        else:
            layers = [create_layer_from_dict(taper['layers'][0])]
        delta = taper['delta_halfgap']
        sum_delta += delta
        length = taper['length']

        for comp_id in component_ids:
            if delta == 0.:
                taper_RW_components.append(flat_part_RW_impedance(
                    comp_id, half_gap=halfgap_tot, length=length,
                    relativistic_gamma=relativistic_gamma,
                    layers=layers, asymmetry_factor=asymmetry_factor))
            else:
                taper_RW_components.append(single_taper_RW_impedance(
                    comp_id, half_gap_small=halfgap_tot,
                    half_gap_big=sum_delta, length=length,
                    relativistic_gamma=relativistic_gamma,
                    layers=layers, asymmetry_factor=asymmetry_factor))
        halfgap_tot += delta

    return taper_RW_components


def halfgap_dependent_hom(halfgap, hom_filename):

    hom_file_path = os.path.join(data_directory, 'collimators', hom_filename)

    # if the file does not exist in lhc_pywit_model/data/collimators,
    # we use it as an absolute path
    if not os.path.exists(hom_file_path):
        hom_file_path = hom_filename

    with open(hom_file_path) as f:
        input_dict = json.load(f)

    # get the hom components
    components = list(set(comp for k, v in input_dict.items() for comp in v))

    hom_dict = {}

    for component in components:
        halfgaps, rqf_dicts = zip(*sorted([
            (float(k) / 2, v[component])
            for k, v in input_dict.items() if component in v]))
        halfgaps = np.array(halfgaps)

        r = np.interp(halfgap, halfgaps, np.array([rqf['r'] for rqf in rqf_dicts]))
        q = np.interp(halfgap, halfgaps, np.array([rqf['q'] for rqf in rqf_dicts]))
        f = np.interp(halfgap, halfgaps, np.array([rqf['f'] for rqf in rqf_dicts]))

        hom_dict[component] = {
            'r': r,
            'q': q,
            'f': f
        }

    return hom_dict
