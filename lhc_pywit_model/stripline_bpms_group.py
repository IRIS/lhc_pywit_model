from pywit.element import Element
from pywit.component import Component
from pywit.elements_group import ElementsGroup

import numpy as np


class StriplineBPMsGroup(ElementsGroup):
    def __init__(self, parameters_dict, betas_lengths_dict, name):
        """
        A group of stripline BPMs
        :param parameters_dict: a dictionary associating to each stripline BPM it's parameters.
        E.g.:
        parameters_dict = {
            ...
            "name": {"length": ..., "angle": ..., "characteristic_imp": ..., "pipe_radius": ...}
            ...
        }
        :param betas_lengths_dict: A dictionary associating to each device a dictionary containing
        the beta_x and beta_y functions and the length of the device (if there are multiple devices with the same
        broadband impedance, e.g. many RF cavities, it will have to indicate the cumulative length of all such devices).
        E.g.: betas_lengths_dict = {
        ...
            'device_name': {'length': ..., 'beta_x': ..., 'beta_y': ...}
        ...
        }
        """
        elements_list = []
        for name in parameters_dict.keys():
            elements_list.append(
                StriplineBPM(electrode_length=parameters_dict['electrode_length'],
                             angle=parameters_dict['angle'],
                             characteristic_imp=parameters_dict['characteristic_imp'],
                             pipe_radius=parameters_dict['pipe_radius'],
                             beta_x=betas_lengths_dict['beta_x'],
                             beta_y=betas_lengths_dict['beta_y'],
                             length=betas_lengths_dict['length'],
                             name=name)
            )
        super().__init__(elements_list=elements_list, name=name)


class StriplineBPM(Element):
    def __init__(self, electrode_length: float, angle: float, characteristic_imp: float, pipe_radius: float,
                 beta_x: float, beta_y: float, length: float, name: str = ''):
        """
        An element representing a stripline bpm using Ng's formula as quoted in Handbook of Accelerator Physics and
        Engr., sec 3.2 (Impedances and wakes functions). NB: only valid for a pair of striplines in symmetric position.
        :param electrode_length: electrode length of strips (=electrodes),
        :param angle: azimuthal angle over which each of them is seen
        :param characteristic_imp: characterisitc impedance of transmission lines from strips (usually 50 Ohm)
        :param pipe_radius: the pipe radius (or half distance between strips),
        :param beta_x: The size of the beta function in the x-plane at the position of the Element
        :param beta_y: The size of the beta function in the y-plane at the position of the Element
        :param length: The length of the element
        :param name: A user-specified name of the Element
        """
        self.electrode_length = electrode_length
        self.angle = angle
        self.characteristic_imp = characteristic_imp
        self.pipe_radius = pipe_radius

        long_comp = Component(impedance=self.longitudinal_impedance_stripline_bpm_ng, wake=lambda x: 0, plane='z',
                              source_exponents=(0, 0), test_exponents=(0, 0))

        xdip_comp = Component(impedance=self.transverse_impedance_stripline_bpm_ng, wake=lambda x: 0, plane='z',
                              source_exponents=(1, 0), test_exponents=(0, 0))

        ydip_comp = Component(impedance=self.transverse_impedance_stripline_bpm_ng, wake=lambda x: 0, plane='z',
                              source_exponents=(0, 1), test_exponents=(0, 0))

        super().__init__(beta_x=beta_x, beta_y=beta_y, length=length, components=[long_comp, xdip_comp, ydip_comp],
                         name=name)

    def longitudinal_impedance_stripline_bpm_ng(self, frequency):
        k = 2. * np.pi * frequency / np.c
        return (2 * (np.sin(k * self.electrode_length)) ** 2 +
                1j * np.sin(2 * k * self.electrode_length)) * self.characteristic_imp * (self.angle / (2 * np.pi)) ** 2

    def transverse_impedance_stripline_bpm_ng(self, frequency):
        k = 2. * np.pi * frequency / np.c
        return (self.longitudinal_impedance_stripline_bpm_ng(frequency) *
                (4 * np.sin(self.angle / 2.) / (self.pipe_radius * self.angle)) ** 2 / k)
