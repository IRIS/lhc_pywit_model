from pywit.model import Model
from pywit.utilities import create_resonator_element

from lhc_pywit_model.collimators_group import CollimatorsGroup
from lhc_pywit_model.roman_pots_group import RomanPotsGroup
from lhc_pywit_model.elliptic_elements_group import EllipticElementsGroup
from lhc_pywit_model.broadband_resonators_group import BroadbandResonatorsGroup
from lhc_pywit_model.resonators_group import ResonatorsGroup
from lhc_pywit_model.stripline_bpms_group import StriplineBPMsGroup
from lhc_pywit_model.rf_cavity import RFCavity
from lhc_pywit_model.utils import compute_betas_and_lengths, execute_jobs_on_htcondor
from lhc_pywit_model.parameters import DEFAULT_RESONATOR_F_ROI_LEVEL, BENDING_RADIUS
from lhc_pywit_model.package_paths import data_directory

from typing import List, Union, Callable
from pathlib import Path
import json
import os

import numpy as np
from scipy.constants import physical_constants, c

import pyoptics


class LHCModel(Model):
    def __init__(self, optics_filename: Union[str, Path] = None, collimators_settings: Union[str, Path] = None,
                 lxplusbatch: str = None,
                 compute_wake: bool = False, f_params_dict: dict = None,
                 z_params_dict: dict = None,
                 f_cutoff_bb: float = 50e9, roman_pots_settings: Union[str, Path] = None,
                 beam_screen_parameters_filename: Union[str, Path] = None,
                 triplets_beam_screen_parameters_filename: Union[str, Path] = None, machine: str = 'LHC',
                 various_elements_parameters_filename: Union[str, Path] = None,
                 tapers_broadband_contributions_parameters_filename: Union[str, Path] = None,
                 bpms_broadband_contributions_parameters_filename: Union[str, Path] = None,
                 other_broadband_contributions_parameters_filename: Union[str, Path] = None,
                 add_design_broadband_contribution: bool = True,
                 homs_parameters_filename_list: List[Union[str, Path]] = None,
                 mki_homs_parameters_filename: Union[str, Path] = None,
                 additional_f_params: dict = None,
                 add_rfd_crab_cavities: bool = False, add_dqw_crab_cavities: bool = False,
                 dqw_homs_file_name: Union[str, Path] = None, rfd_homs_file_name: Union[str, Path] = None,
                 apply_homs_frequency_spread: bool = True, homs_spread_range: float = 3e6,
                 ccs_feedback_type: str = None, bblrc_stripline_parameters_filename: Union[str, Path] = None,
                 jobs_submission_function: Callable = execute_jobs_on_htcondor, flag_hllhc: bool = False,
                 normalized_emittance_for_coll: float = 3.5, compute_geometric_impedance_collimators: bool = True,
                 compute_taper_RW_impedance_collimators: bool = True,
                 use_single_layer_approx_for_coated_taper: bool = True,
                 resonator_f_roi_level: float = DEFAULT_RESONATOR_F_ROI_LEVEL,
                 b_field_arcs: Union[float, None] = None, b_field_triplets: Union[float, None] = None,
                 h_lhc: int = 35640, frequency_parameters_for_taper_rw: dict = None, NCCs: int = 4,
                 rfd_name_in_twiss_file: str = 'acfgav', dqw_name_in_twiss_file: str = 'acfgah',
                 rotation_angle_dqw_homs: float = 0  # this parameters should not be used. It is only useful to compare
                                                     # with the old model
                                                     # (see https://gitlab.cern.ch/IRIS/lhc_pywit_model/-/issues/35)
                 ):

        self.machine = machine
        self.optics_filename = optics_filename

        # If we want to use the lumped betas then we have to compute the average betas
        tfs = pyoptics.optics.open(self.optics_filename)
        circ = tfs.header['length']
        radius = circ / (2 * np.pi)
        q_x = tfs.header['q1']
        q_y = tfs.header['q2']

        self.beta_x_smooth = radius / q_x
        self.beta_y_smooth = radius / q_y

        relativistic_gamma = tfs.header['gamma']
        relativistic_beta = np.sqrt(1 - 1 / tfs.header['gamma']**2)

        self.betas_lengths_dict = compute_betas_and_lengths(optics_filename, flag_hllhc)

        self.resonator_f_roi_level = resonator_f_roi_level

        elements_list = []

        if collimators_settings:
            taper_settings_filename = os.path.join(data_directory, 'collimators', 'taper_data.json')

            elements_list.append(CollimatorsGroup(
                settings_filename=collimators_settings, lxplusbatch=lxplusbatch, relativistic_gamma=relativistic_gamma,
                normalized_emittance=normalized_emittance_for_coll,
                compute_wake=compute_wake,
                f_params_dict=f_params_dict, z_params_dict=z_params_dict,
                f_cutoff_bb=f_cutoff_bb,
                name='collimators', machine=machine,
                compute_geometric_impedance=compute_geometric_impedance_collimators,
                compute_taper_RW_impedance=compute_taper_RW_impedance_collimators,
                use_single_layer_approx_for_coated_taper=use_single_layer_approx_for_coated_taper,
                optics_filename=optics_filename,
                additional_f_params=additional_f_params,
                jobs_submission_function=jobs_submission_function, taper_settings=taper_settings_filename,
                resonator_f_roi_level=self.resonator_f_roi_level,
                frequency_parameters_for_taper_rw=frequency_parameters_for_taper_rw))

        if roman_pots_settings:
            elements_list.append(RomanPotsGroup(
                settings_filename=roman_pots_settings, lxplusbatch=lxplusbatch, relativistic_gamma=relativistic_gamma,
                compute_wake=compute_wake,
                f_params_dict=f_params_dict, z_params_dict=z_params_dict,
                name='roman pots', machine=machine,
                jobs_submission_function=jobs_submission_function))

        yokoya_factors_beam_screen_filename = os.path.join(data_directory, 'elliptic_elements',
                                                           'Yokoya_factors_elliptic.dat')

        pumping_holes_parameters_filename = os.path.join(data_directory, 'pumping_holes',
                                                         'lhc_pumping_holes.json')

        if beam_screen_parameters_filename:
            if b_field_arcs is None:
                energy = relativistic_gamma * physical_constants['proton mass energy equivalent in MeV'][0] * 1e6
                b_field_arcs = energy / (c * BENDING_RADIUS)

            elements_list.append(EllipticElementsGroup(
                lxplusbatch=lxplusbatch, parameters_filename=beam_screen_parameters_filename,
                betas_lengths_dict=self.betas_lengths_dict, f_params_dict=f_params_dict, z_params_dict=z_params_dict,
                relativistic_gamma=relativistic_gamma, machine=machine,
                compute_wake=compute_wake,
                yokoya_factors_filename=yokoya_factors_beam_screen_filename, name='beam screen',
                pumping_holes_parameters_filename=pumping_holes_parameters_filename,
                pumping_holes_model='mostacci', f_cutoff=f_cutoff_bb,
                b_field=b_field_arcs, additional_f_params=additional_f_params,
                jobs_submission_function=jobs_submission_function))

        triplets_pumping_holes_parameters_filename = os.path.join(data_directory, 'pumping_holes',
                                                                  'hllhc_pumping_holes_triplets.json')

        if triplets_beam_screen_parameters_filename:
            if b_field_triplets is None:
                # for the triplets by default we still compute the B field computed from the energy with
                # LHC bending radius, which is not really correct, but it has a small effect anyway...
                energy = relativistic_gamma * physical_constants['proton mass energy equivalent in MeV'][0] * 1e6
                b_field_triplets = energy / (c * BENDING_RADIUS)

            elements_list.append(EllipticElementsGroup(
                lxplusbatch=lxplusbatch,
                parameters_filename=triplets_beam_screen_parameters_filename,
                betas_lengths_dict=self.betas_lengths_dict, f_params_dict=f_params_dict, z_params_dict=z_params_dict,
                relativistic_gamma=relativistic_gamma, machine=machine,
                compute_wake=compute_wake,
                yokoya_factors_filename=yokoya_factors_beam_screen_filename, name='triplets beam screen',
                pumping_holes_parameters_filename=triplets_pumping_holes_parameters_filename,
                pumping_holes_model='mostacci', b_field=b_field_triplets, additional_f_params=additional_f_params,
                jobs_submission_function=jobs_submission_function))

        if various_elements_parameters_filename:
            elements_list.append(EllipticElementsGroup(
                lxplusbatch=lxplusbatch, parameters_filename=various_elements_parameters_filename,
                betas_lengths_dict=self.betas_lengths_dict, f_params_dict=f_params_dict, z_params_dict=z_params_dict,
                relativistic_gamma=relativistic_gamma, machine=machine,
                compute_wake=compute_wake,
                yokoya_factors_filename=yokoya_factors_beam_screen_filename, name='various elliptic RW elements',
                additional_f_params=additional_f_params, jobs_submission_function=jobs_submission_function))

        if tapers_broadband_contributions_parameters_filename:
            with open(tapers_broadband_contributions_parameters_filename) as tapers_file:
                tapers_broadband_contributions_parameters_dict = json.load(tapers_file)

            elements_list.append(BroadbandResonatorsGroup(
                betas_lengths_dict=self.betas_lengths_dict,
                parameters_dict=tapers_broadband_contributions_parameters_dict,
                name='tapers broadband contributions'))

        if bpms_broadband_contributions_parameters_filename:
            with open(bpms_broadband_contributions_parameters_filename) as bpms_file:
                bpms_broadband_contributions_parameters_dict = json.load(bpms_file)

            elements_list.append(BroadbandResonatorsGroup(
                betas_lengths_dict=self.betas_lengths_dict,
                parameters_dict=bpms_broadband_contributions_parameters_dict,
                name='BPMs broadband contributions',
                f_cutoff_broadband=f_cutoff_bb))

        if other_broadband_contributions_parameters_filename:
            with open(other_broadband_contributions_parameters_filename) as other_bb_file:
                other_broadband_contributions_parameters_dict = json.load(other_bb_file)

            elements_list.append(BroadbandResonatorsGroup(
                betas_lengths_dict=self.betas_lengths_dict,
                parameters_dict=other_broadband_contributions_parameters_dict,
                name='other broadband contributions',
                f_cutoff_broadband=f_cutoff_bb))

        if bblrc_stripline_parameters_filename:
            with open(bblrc_stripline_parameters_filename, 'r') as bblrc_striple_parameters_file:
                bblrc_striple_parameters_dict = json.load(bblrc_striple_parameters_file)

            elements_list.append(StriplineBPMsGroup(parameters_dict=bblrc_striple_parameters_dict,
                                                    betas_lengths_dict=self.betas_lengths_dict, name='BBLRCs'))

        if homs_parameters_filename_list:
            for homs_parameters_filename in homs_parameters_filename_list:
                parameters_filename = homs_parameters_filename
                with open(parameters_filename) as parameters_file:
                    parameters_dict = json.load(parameters_file)

                elements_list.append(ResonatorsGroup(
                    betas_lengths_dict=self.betas_lengths_dict,
                    parameters_dict=parameters_dict,
                    name=homs_parameters_filename.strip('.json'),
                    resonator_f_roi_level=self.resonator_f_roi_level))

        if mki_homs_parameters_filename:
            parameters_filename = mki_homs_parameters_filename
            with open(parameters_filename) as parameters_file:
                parameters_dict = json.load(parameters_file)

            elements_list.append(ResonatorsGroup(
                betas_lengths_dict=self.betas_lengths_dict,
                parameters_dict=parameters_dict,
                name='mki HOMs',
                resonator_f_roi_level=self.resonator_f_roi_level))

        for i_elem, elem in enumerate(elements_list):
            for i_comp, comp in enumerate(elem.components):
                if not compute_wake:
                    elem.components[i_comp].wake = lambda x: 0

            if hasattr(elem, 'elements_list'):
                for i_elem_inner, elem_inner in enumerate(elements_list[i_elem].elements_list):
                    for i_comp_inner, comp_inner in enumerate(elem_inner.components):
                        if not compute_wake:
                            elem.elements_list[i_elem_inner].components[i_comp_inner].wake = lambda x: 0

        if add_design_broadband_contribution:
            rs = {'z0000': 10.5e3 * f_cutoff_bb / 5e9, 'x1000': 0.67e6, 'y0100': 0.67e6}
            qs = {'z0000': 1, 'x1000': 1, 'y0100': 1}
            fs = {'z0000': f_cutoff_bb, 'x1000': f_cutoff_bb, 'y0100': f_cutoff_bb}
            # The broadband impedance is already weighted by the beta functions so we set the length to 1e-12 to skip
            # the beta weighting in pywit. We cannot
            elements_list.append(create_resonator_element(
                length=1e-12, beta_x=self.beta_x_smooth, beta_y=self.beta_y_smooth, rs=rs, qs=qs, fs=fs,
                description='broadband contribution from the design report'))

            elements_list[-1].name = 'design broadband'

        # some crab cavities parameters
        t_rev = circ / (relativistic_beta * c)
        fund_mode_filename = os.path.join(data_directory, 'crab_cavities',
                                          'crab_cavities_fundamental_mode_parameters.json')

        with open(fund_mode_filename, 'r') as data_file:
            fund_mode_dict = json.load(data_file)

        shunt_imp_ccs = fund_mode_dict['shunt_impedance']
        q_factor_ccs = fund_mode_dict['quality_factor']
        fund_freq_ccs = h_lhc / t_rev

        if ccs_feedback_type == 'standard':
            feedback_filename = os.path.join(data_directory, 'crab_cavities',
                                             'crab_cavities_standard_transverse_rf_feedback_parameters.json')
            with open(feedback_filename, 'r') as data_file:
                feedback_dict = json.load(data_file)
        elif ccs_feedback_type == 'comb':
            feedback_filename = os.path.join(data_directory, 'crab_cavities',
                                             'crab_cavities_betatron_comb_filter_feedback_parameters.json')
            with open(feedback_filename, 'r') as data_file:
                feedback_dict = json.load(data_file)
        else:
            feedback_dict = {}

        if add_rfd_crab_cavities:
            # assuming that the crossing plane in point 1 is h and in point 5 is v
            inds_rfd = [name.startswith(rfd_name_in_twiss_file) and '1.' in name for name in tfs.name]

            if sum(inds_rfd) == 0:
                raise ValueError('No RFD cavities found in twiss file')

            self.beta_x_rfd = np.mean(tfs.betx[inds_rfd])
            self.beta_y_rfd = np.mean(tfs.bety[inds_rfd])

            if rfd_homs_file_name == 'default':
                rfd_homs_file_name = os.path.join(data_directory, 'crab_cavities',
                                                  'crab_cavities_rfd_homs.json')

            rs_fundamental_mode = {'x1000': shunt_imp_ccs}
            qs_fundamental_mode = {'x1000': q_factor_ccs}
            fs_fundamental_mode = {'x1000': fund_freq_ccs}

            elem_rfd = RFCavity(rs_fundamental_mode=rs_fundamental_mode, qs_fundamental_mode=qs_fundamental_mode,
                                fs_fundamental_mode=fs_fundamental_mode, homs_file_name=rfd_homs_file_name,
                                length=1e-12, beta_x=self.beta_x_rfd, beta_y=self.beta_y_rfd,
                                apply_homs_frequency_spread=apply_homs_frequency_spread,
                                homs_spread_range=homs_spread_range, homs_rng_seed=1,
                                add_transverse_feedback_x=ccs_feedback_type is not None,
                                transverse_feedback_type_x=ccs_feedback_type, name='RF dipole Crab Cavity',
                                tune_x=q_x, t_rev=t_rev, resonator_f_roi_level=self.resonator_f_roi_level, ncav=NCCs,
                                **feedback_dict
                                )

            elements_list.append(elem_rfd)

        if add_dqw_crab_cavities:
            inds_dqw = [name.startswith(dqw_name_in_twiss_file) and '5.' in name for name in tfs.name]

            if sum(inds_dqw) == 0:
                raise ValueError('No DQW cavities found in twiss file')

            self.beta_x_dqw = np.mean(tfs.betx[inds_dqw])
            self.beta_y_dqw = np.mean(tfs.bety[inds_dqw])

            if dqw_homs_file_name == 'default':
                dqw_homs_file_name = os.path.join(data_directory, 'crab_cavities',
                                                  'crab_cavities_dqw_homs.json')

            rs_fundamental_mode = {'y0100': shunt_imp_ccs}
            qs_fundamental_mode = {'y0100': q_factor_ccs}
            fs_fundamental_mode = {'y0100': fund_freq_ccs}

            elem_dqw = RFCavity(rs_fundamental_mode=rs_fundamental_mode, qs_fundamental_mode=qs_fundamental_mode,
                                fs_fundamental_mode=fs_fundamental_mode, homs_file_name=dqw_homs_file_name,
                                length=1e-12, beta_x=self.beta_x_dqw, beta_y=self.beta_y_dqw,
                                apply_homs_frequency_spread=apply_homs_frequency_spread,
                                homs_spread_range=homs_spread_range, homs_rng_seed=2,
                                add_transverse_feedback_y=ccs_feedback_type is not None,
                                transverse_feedback_type_y=ccs_feedback_type, name='DQW Crab Cavity',
                                tune_y=q_y, t_rev=t_rev, resonator_f_roi_level=self.resonator_f_roi_level, ncav=NCCs,
                                rotation_angle_homs=rotation_angle_dqw_homs, **feedback_dict
                                )

            elements_list.append(elem_dqw)

        super().__init__(elements=elements_list, lumped_betas=(self.beta_x_smooth, self.beta_y_smooth))
