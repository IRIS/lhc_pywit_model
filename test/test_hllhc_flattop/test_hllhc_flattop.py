from lhc_pywit_model.lhc_model import LHCModel
from lhc_pywit_model.utils import execute_jobs_locally
from lhc_pywit_model.package_paths import data_directory

import numpy as np
import os
from pathlib import Path


def test_hllhc_flattop():
    test_freqs = 10 ** np.linspace(3, 10, 8)
    test_folder = Path(__file__).parent

    f_params_dict = {'start': 1e2,
                     'stop': 1e11,
                     'scan_type': 0,
                     'added': '',
                     'points_per_decade': 1
                     }

    z_params_dict = {'start': 0.01,
                     'stop': 1e13,
                     'scan_type': 2,
                     'added': ''
                     }

    resonators_directory = os.path.join(data_directory, 'resonators')
    broadband_directory = os.path.join(data_directory, 'broadband_resonators')
    elliptic_elements_directory = os.path.join(data_directory, 'elliptic_elements')
    optics_directory = os.path.join(data_directory, 'optics')
    collimators_directory = os.path.join(data_directory, 'collimators')

    input_params = {
        'optics_filename': os.path.join(optics_directory, 'twiss_hllhcb1_beta100cm.tfs'),
        'collimators_settings': os.path.join(collimators_directory, 'hllhc_collimators_reference_b1.json'),
        'lxplusbatch': None,
        'compute_wake': False,
        'f_params_dict': f_params_dict,
        'z_params_dict': z_params_dict,
        'f_cutoff_bb': 50e9,
        'compute_geometric_impedance_collimators': True,
        'roman_pots_settings': None,
        'beam_screen_parameters_filename': os.path.join(elliptic_elements_directory,
                                                        'hllhc_rw_parameters_beam_screen.json'),
        'triplets_beam_screen_parameters_filename': os.path.join(elliptic_elements_directory,
                                                                 'hllhc_rw_parameters_beam_screen_triplets.json'),
        'machine': 'LHC',
        'various_elements_parameters_filename': os.path.join(elliptic_elements_directory,
                                                             'lhc_rw_parameters_various_elements.json'),
        'tapers_broadband_contributions_parameters_filename': os.path.join(broadband_directory,
                                                                           'hllhc_triplets_tapers_broadband.json'),
        'bpms_broadband_contributions_parameters_filename':  os.path.join(broadband_directory,
                                                                          'hllhc_triplets_bpms_broadband.json'),
        'other_broadband_contributions_parameters_filename': os.path.join(broadband_directory,
                                                                          'hllhc_triplets_other_broadband.json'),
        'add_design_broadband_contribution': True,
        'homs_parameters_filename_list': [os.path.join(resonators_directory, 'lhc_alice_homs.json'),
                                          os.path.join(resonators_directory, 'hllhc_cms_homs.json'),
                                          os.path.join(resonators_directory, 'hllhc_atlas_homs.json'),
                                          os.path.join(resonators_directory, 'lhc_lhcb_homs.json'),
                                          os.path.join(resonators_directory, 'lhc_rf_cavities_homs.json'),
                                          os.path.join(resonators_directory, 'lhc_velo_homs.json')
                                          ],
        'mki_homs_parameters_filename': os.path.join(resonators_directory,
                                                     'hllhc_mkicool_homs_2018_10_22_from_VV.json'),
        'flag_hllhc': True,
        'add_rfd_crab_cavities': True,
        'add_dqw_crab_cavities': True,
        'dqw_homs_file_name': 'default',
        'rfd_homs_file_name': 'default',
        'ccs_feedback_type': 'comb',
        'bblrc_stripline_parameters_filename': None,
        'use_single_layer_approx_for_coated_taper': True,
        'jobs_submission_function': execute_jobs_locally
    }

    hllhc_model = LHCModel(**input_params)

    reference_files_dir = os.path.join(test_folder, 'reference_files')
    comp_id_filename_map = {
        'z0000': os.path.join(reference_files_dir, 'Zlong_reference_test.dat'),
        'x1000': os.path.join(reference_files_dir, 'Zxdip_reference_test.dat'),
        'y0100': os.path.join(reference_files_dir, 'Zydip_reference_test.dat'),
    }

    elem_tot = hllhc_model.total

    for comp_id in comp_id_filename_map:
        print(comp_id)
        z_vs_freq = elem_tot.get_component(comp_id).discretize(8, 1, 1e3, 1e10, freq_precision_factor=0.8)[0]
        sorted_indices = np.argsort(z_vs_freq[0])

        freqs_sorted = z_vs_freq[0][sorted_indices]
        z_sorted = z_vs_freq[1][sorted_indices]

        z_test_re = np.interp(test_freqs, freqs_sorted, np.real(z_sorted))
        z_test_im = np.interp(test_freqs, freqs_sorted, np.imag(z_sorted))

        # use this line to produce ref files
        # np.savetxt(comp_id_filename_map[comp_id], np.vstack((test_freqs, z_test_re, z_test_im)).T)

        # open files
        z_ref = np.loadtxt(comp_id_filename_map[comp_id], dtype=float)

        z_ref_re = np.interp(test_freqs, z_ref[:, 0], z_ref[:, 1])
        z_ref_im = np.interp(test_freqs, z_ref[:, 0], z_ref[:, 2])

        assert np.allclose(z_ref_re, z_test_re)

        assert np.allclose(z_ref_im, z_test_im)
