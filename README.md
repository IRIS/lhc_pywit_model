# LHC_PyWIT_model

This code allows computing the beam-coupling impedance of the Large Hadron Collider [[1]](#1).


## References
<a id="1">[1]</a> 
N. Mounet (2012). 
The LHC Transverse Coupled-Bunch Instability. 
[https://cds.cern.ch/record/1451296](https://cds.cern.ch/record/1451296)

## Installation

To install the code it is enough to clone the repository and run `pip install .` in the base directory.

In order to download collimator settings directly from timber using the function `lhc_coll_settings_from_timber`, the
pytimber package must be installed. On a generic Miniconda environment this can be done as follows:

```
# Change python package index to CERN index
pip install git+https://gitlab.cern.ch/acc-co/devops/python/acc-py-pip-config.git

# Install pytimber
pip install pytimber

# Change python package index back to default
pip uninstall acc-py-pip-config
```

## Usage

An example of usage of the code can be found in `examples/test_lhc_flattop`

## Disclaimer

The code is still in beta status and it is still subject to changes.
On the other hand, the tests results agree well with the old impedance model, therefore the underlying physics is not expected to change drastically.
