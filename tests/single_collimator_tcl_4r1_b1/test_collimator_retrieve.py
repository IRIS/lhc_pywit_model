import sys
sys.path.append('../../source')

from lhc_pywit_model.collimators_group import CollimatorsGroup

import numpy as np
import os


def test_single_collimator_tcl_4r1_b1_retrieve():
    f_params_dict = {'start': 10,
                     'stop': 1e13,
                     'scan_type': 0,
                     'added': '1e-2 0.1 1 1e15',
                     'points_per_decade': 10,
                     'min_refine': 1e11,
                     'max_refine': 5e12,
                     'n_refine': 5000
                     }

    z_params_dict = {'start': 0.01,
                     'stop': 1e13,
                     'scan_type': 2,
                     'added': ''
                     }

    avbetax = 68.0932924128
    avbetay = 70.3397386547

    collimators_group = CollimatorsGroup(settings_filename='./data/only_TCL.4R1.B1.json',
                                         lxplusbatch='retrieve', relativistic_gamma=7247.36542615,
                                         jobs_input_files_directory='test_pywit_singlecoll', 
                                         path_to_iw2d_executables=os.getenv('IW2D_EXEC_PATH'),
                                         compute_wake=False, scenario='test',
                                         f_params_dict=f_params_dict, z_params_dict=z_params_dict, 
                                         submit_jobs=True, force_recompute_scenario=True, 
                                         f_cutoff_bb=5e10)
    collimators_group.launch_or_retrieve()

    dict_components = {
                       'zxdip': 'x1000',
                       'zydip': 'y0100',
                       'zxqua': 'x0010',
                       'zyqua': 'y0001',
                       'zlong': 'z0000'
                      }

    for component in dict_components.keys():
        print('testing ' + component)
        old_results_file_geom = f'data/{component}_TCL.4R1.B1_Geom.dat'
        old_results_table_geom = np.loadtxt(old_results_file_geom, skiprows=1)
        breakpoint()
        pywit_component = collimators_group.elements_list[0].get_component(dict_components[component])
        new_impedance_table = pywit_component.impedance(old_results_table_geom[:, 0])
        new_impedance_table_re = np.real(new_impedance_table)
        new_impedance_table_im = np.imag(new_impedance_table)

        if dict_components[component].startswith('x'):
            new_results_table_re = new_impedance_table_re*(collimators_group.elements_list[1].beta_x/avbetax)
            new_results_table_im = new_impedance_table_im*(collimators_group.elements_list[1].beta_x/avbetax)
        elif dict_components[component].startswith('y'):
            new_results_table_re = new_impedance_table_re*(collimators_group.elements_list[1].beta_y/avbetay)
            new_results_table_im = new_impedance_table_im*(collimators_group.elements_list[1].beta_y/avbetay)
        else:
            new_results_table_re = new_impedance_table_re
            new_results_table_im = new_impedance_table_im

        assert np.isclose(old_results_table_geom[:,1], new_results_table_re).all()
        assert np.isclose(old_results_table_geom[:,2], new_results_table_im).all()
