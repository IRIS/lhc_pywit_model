import sys
sys.path.append('../../source')

from lhc_pywit_model.collimators_group import CollimatorsGroup

import numpy as np
import os


def iw2d_inputs_to_dict(filename):
    dict_inputs = {}
    with open(filename) as file_iw2d:
        lines = file_iw2d.readlines()
        for line in lines:
            if line.startswith('Comments'):
                continue
            elif line.startswith('added frequencies'):
                dict_inputs[line.split(':')[0]] = list(map(float, line.split(':')[1][1:].split(' ')))
            else:
                try:
                    dict_inputs[line.split(':')[0]] = float(line.split(':')[1])
                except:            
                    dict_inputs[line.split(':')[0]] = line.split(':')[1]

    return dict_inputs


def compare_iw2d_dicts(dict_ref, dict_test):
    for key in dict_ref.keys():
        if key not in dict_test.keys():
            print(key + ' not found in iw2d input file')
            return False
        if type(dict_test[key]) == float:
            if not np.isclose(dict_test[key], dict_ref[key]):
                print(key + ' has wrong value')
                return False
        else:
            if dict_test[key] != dict_ref[key]:
                print(key + ' has wrong value')
                return False 
    return True


def test_single_collimator_tcl_4r1_b1_launch():
    f_params_dict = {'start': 10,
                     'stop': 1e13,
                     'scan_type': 0,
                     'added': '1e-2 0.1 1 1e15',
                     'points_per_decade': 10,
                     'min_refine': 1e11,
                     'max_refine': 5e12,
                     'n_refine': 5000,
                     'sampling_exponent': 1e8,
                     }

    z_params_dict = {'start': 0.01,
                     'stop': 1e13,
                     'scan_type': 2,
                     'added': ''
                     }

    collimators_group = CollimatorsGroup(settings_filename='./data/only_TCL.4R1.B1.json',
                                         lxplusbatch='launch', relativistic_gamma=7247.36542615,
                                         jobs_input_files_directory='./test_pywit_singlecoll',
                                         path_to_iw2d_executables=os.getenv('IW2D_EXEC_PATH'),
                                         compute_wake=False, scenario='test',
                                         f_params_dict=f_params_dict, z_params_dict=z_params_dict,
                                         submit_jobs=False, force_recompute_scenario=True,
                                         f_cutoff_bb=5e10)

    collimators_group.launch_or_retrieve()
    dict_ref = iw2d_inputs_to_dict('./data/IW2D_input_TCL.4R1.B1_CU_sym_singlecoll.txt')
    dict_test = iw2d_inputs_to_dict('./test_pywit_singlecoll/collimators/test/RW/TCL.4R1.B1/'+
                                    'iw2d_input.txt')

    assert compare_iw2d_dicts(dict_ref, dict_test)    
