Machine:	LHC
Relativistic Gamma:	7247.36542615
Impedance Length in m:	21992.0346
Number of layers:	2
Layer 1 inner radius in mm:	18.375
Layer 1 DC resistivity (Ohm.m):	7.727798143377088e-10
Layer 1 relaxation time for resistivity (ps):	0.5516702093434602
Layer 1 real part of dielectric constant:	1.0
Layer 1 magnetic susceptibility:	0.0
Layer 1 relaxation frequency of permeability (MHz):	inf
Layer 1 thickness in mm:	0.05
Layer 2 DC resistivity (Ohm.m):	6e-07
Layer 2 relaxation time for resistivity (ps):	0.0
Layer 2 real part of dielectric constant:	1.0
Layer 2 magnetic susceptibility:	0.0
Layer 2 relaxation frequency of permeability (MHz):	inf
Layer 2 thickness in mm:	inf
start frequency exponent (10^) in Hz:	1.0
stop frequency exponent (10^) in Hz:	13.0
linear (1) or logarithmic (0) or both (2) frequency scan:	0
sampling frequency exponent (10^) in Hz (for linear):	8.0
Number of points per decade (for log):	10.0
when both, fmin of the refinement (in THz):	0.1
when both, fmax of the refinement (in THz):	5.0
when both, number of points in the refinement:	5000.0
added frequencies [Hz]:	0.01 0.1 1.0 1000000000000000.0
Yokoya factors long, xdip, ydip, xquad, yquad:	1.0 0.8982468666597658 0.6513157825561054 0.241266832881876 -0.23766835858238128
Comments for the output files names:	test_BS_50A
